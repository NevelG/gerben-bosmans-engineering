# Nevelkamer Beeldherkenning Project Toegepaste Nucleaire Electronica 2

Inhoud
======
  1. inleiding
  2. systeemvereisten
  3. installatie
  4. gebruik programma
  5. FAQ

inleiding
=========
Welkom en proficiat met de juiste keuze om onze software te gebruiken. deze software is ontwikkeld door Nick Staut, Florian Cremelie, Stef Geelen, en Gerben Bosmans. Het team van Gerben Bosmans Engineering wenst u zeer veel succes met de software en de verdere studies. Bij vragen kan u ons bereiken op gerbenbosmansengineering (at) gmail (dot) com

systeemvereisten
================

  Minimumvereisten
  ----------------
  - een computer met een operating system
  - matlab 2014b of nieuwer
  - image processing toolbox
  - een dual core cpu
  - 4GB ram geheugen
  - een duidelijke video van een Nevelkamer


  Aanbevolen vereisten
  --------------------
  - parallel processing toolbox
  - statistics and machine learning toolbox
  - Nvidia gpu met cuda 3.0 of hoger
  - quad core cpu
  - 16GB ram geheugen

Installatie
===========

Het pakketje Nevelkamer.zip moet uitgepakt worden. Hierna moet de locatie waar het uitgepakt is toegevoegd worden aan het path in matlab. dit is alles wat vereist is.

Gebruik Programma
=================
algemene rondleiding
--------------------
om het programma te starten moet men op de juiste launcher klikken. indien je op een windows pc werkt, moet je op NevelLauncherWin.bat klikken. Indien je zo iemand bent die met mac of linux werkt, dan moet je op NevelLauncherUnix.sh klikken.

Linksboven zie je drie dropdownmenu's.
  -exit om het programma te verlaten
  -file om de video te laden, de vooruitgang op te slaan of om vorig werk in te laden
  -results dit geeft de resultaten van de ghosting weer en laat toe dat men nog verdere bewerkingen te doen

daarna vindt men een verzameling van filters, deze laten u toe om het beeld te manipuleren op het rechter scherm.

Daaronder bevinden zich de tools om de video af te spelen en bij te snijden.

Het linker videoscherm toont het originele beeld.

Rechtsboven ziet men twee tekstvakken met betrekking tot ghosting. hier moet men links het eerste frame van het te ghosten bereik ingeven en rechts het laatste van het te ghosten bereik.

hieronder zijn enkele instellingen te vinden voor het filteren van de achtergrond. verder staat er ook nog een checkbox voor live tracking van de stralen in te schakelen.

De background models zijn de volgende:  
  - single frame, gebruikt een frame  
  - averaged frames, berekend het gemiddelde van het bereik van frames  
  - median background model, bepaald de mediane achtergrond  
  - gaussian mixture model, Kan enkel gebruikt worden door eigen implementatie van externe libraries (mexopencv)  

Er zijn vijf verschillende weergavemodi die kunnen gebruikt worden.  
  - Nick's special mix is de standaard weergave en gebruikt de ingestelde manipulaties die linksboven staan.  
  - enhanced is een contrastverhogend beeld waarbij de achtergrond in het groen wordt getoond en de straling in het rood.  
  - background substraction geeft enkel de stralen weer tegen een zwarte achtergrond.  
  - ghosted tracks bewerkt het beeld zodat de stralen live geghost worden.  
  - show background model toont het gegenereerde achtergrondmodel.  
  - show magnet position toont een binaire mask die aangeeft waar de magneet gepositioneerd is.  

het rechterscherm toont het bewerkte beeld.

Onder het videoscherm is een tool waarmee men de regio van de magneet kan aanduiden.

Stappenplan gebruik software
----------------------------
je maakt een filmpje met de nevelkamer. Je zorgt dat de camera die je gebruikt van voldoende resolutie is en dat de kamer voldoende verduisterd is. De stabiliteit van de camera is ook zeer belangrijk.

de bestandsindeling van het filmpje speelt geen rol, indien er echter toch problemen opduiken, dan is het aan te raden om te converteren naar avi.

Eerst laad men het filmpje in, je test of het filmpje start en pauzeert. Daarna zorg je voor het achtergrondmodel. Vaak is de median filter het sterkste voor de achtergrond. Vervolgens doe je de handelingen die je wenst uit te voeren.

Machine learning
----------------
Op de bitbucket staan onder de directory "examples" 2 voorbeeldscripts voor het bouwen van een library voor het leren herkennen van de stralingstypen. Dit is niet verplicht, er zit ook een formule in, deze is echter niet zo nauwkeurig. de machine learning kan zeer handig zijn bij afwijkende filmpjes.

FAQ
===
1. nog geen vragen binnengekregen