function closeSavePopup(this, src, eventdata)

if strcmpi(this.saveHandles.exportPath.String, this.saveHandles.defaultFilePath)
    selection = questdlg('You did not enter a custom filename, save with default name?',...
        'Closing',...
        'Save','Change Filepath','Cancel Saving','Save');
    
    switch selection
        case 'Save'
            % might add some stuff to change the visability
            % of editable stuff or something so you dont
            % have buttons on the result, its quite trivial
            % to apply this but to save some time skipping
            % it now
            try
                set(this.figureHandle, 'paperpositionmode','auto')
                print(this.figureHandle,'-dpng',this.saveHandles.exportPath.String,'-r400','-painters')
                try
                    tableFileName = [this.saveHandles.exportPath.String(1:end-4), '.xlsx'];
                    writableResultTable = array2table(this.resultTable.Data);
                    writableResultTable.Properties.VariableNames{1} = 'RadiationType';
                    writableResultTable.Properties.VariableNames{2} = 'ConfidenceLevel';
                    writableResultTable.Properties.VariableNames{3} = 'TrackPixelArea';
                    writableResultTable.Properties.VariableNames{4} = 'TrackPerimeter';
                    writableResultTable.Properties.VariableNames{5} = 'BoundingBoxVisible';
                    writetable(writableResultTable,tableFileName)
                catch
                    tableFileName = [this.saveHandles.exportPath.String(1:end-4), '.csv '];
                    writableResultTable = array2table(this.resultTable.Data);
                    writableResultTable.Properties.VariableNames{1} = 'RadiationType';
                    writableResultTable.Properties.VariableNames{2} = 'ConfidenceLevel';
                    writableResultTable.Properties.VariableNames{3} = 'TrackPixelArea';
                    writableResultTable.Properties.VariableNames{4} = 'TrackPerimeter';
                    writableResultTable.Properties.VariableNames{5} = 'BoundingBoxVisible';
                    writetable(writableResultTable,tableFileName)
                end
                
                delete(this.saveHandles.popupWindowSaveResults)
            catch
                set(this.figureHandle, 'paperpositionmode','auto')
                print(this.figureHandle,'-dpng',this.saveHandles.defaultFilePath,'-r400','-painters')
                warndlg('encountered error writing file to specified folder, resorted to default filename in current directory')
            end
            delete(this.saveHandles.popupWindowSaveResults)
        case 'Change Filepath'
            return
        case 'Cancel Saving'
            delete(this.saveHandles.popupWindowSaveResults)
    end
else
    try
        set(this.figureHandle, 'paperpositionmode','auto')
        print(this.figureHandle,'-dpng',this.saveHandles.exportPath.String,'-r400','-painters')
        try
            tableFileName = [this.saveHandles.exportPath.String(1:end-4), '.xlsx'];
            writableResultTable = array2table(this.resultTable.Data);
            writableResultTable.Properties.VariableNames{1} = 'RadiationType';
            writableResultTable.Properties.VariableNames{2} = 'ConfidenceLevel';
            writableResultTable.Properties.VariableNames{3} = 'TrackPixelArea';
            writableResultTable.Properties.VariableNames{4} = 'TrackPerimeter';
            writableResultTable.Properties.VariableNames{5} = 'BoundingBoxVisible';
            writetable(writableResultTable,tableFileName)
        catch
            tableFileName = [this.saveHandles.exportPath.String(1:end-4), '.csv '];
            writableResultTable = array2table(this.resultTable.Data);
            writableResultTable.Properties.VariableNames{1} = 'RadiationType';
            writableResultTable.Properties.VariableNames{2} = 'ConfidenceLevel';
            writableResultTable.Properties.VariableNames{3} = 'TrackPixelArea';
            writableResultTable.Properties.VariableNames{4} = 'TrackPerimeter';
            writableResultTable.Properties.VariableNames{5} = 'BoundingBoxVisible';
            writetable(writableResultTable,tableFileName)
        end
        
        delete(this.saveHandles.popupWindowSaveResults)
    catch
        set(this.figureHandle, 'paperpositionmode','auto')
        print(this.figureHandle,'-dpng',this.saveHandles.defaultFilePath,'-r400','-painters')
        warndlg('encountered error writing file to specified folder, resorted to default filename in current directory')
    end
    
end
end