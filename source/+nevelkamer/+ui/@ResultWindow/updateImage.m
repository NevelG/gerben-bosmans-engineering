function updateImage(this, src, eventdata)

if this.dropdown.displayChoice.Value == 2
    outputImage= uint8(this.originalFrames(:,:,:,round(this.slider.frames.Value)));
else
    outputImage= this.processedGhostedImage;
end
imshow(outputImage,'Parent',this.imageContainer);

if this.interactiveAnalysis
   this.analyseTracks; 
end

end

