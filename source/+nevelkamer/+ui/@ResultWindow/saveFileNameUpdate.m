function saveFileNameUpdate(this, src, eventdata)
[pathname, filename, extentiontype]=  fileparts(this.saveHandles.saveFileName.String);
if strcmp(extentiontype, '.png')
    this.saveHandles.exportPath.String = [this.saveHandles.saveFilePath.String,this.saveHandles.saveFileName.String];
else
    this.saveHandles.exportPath.String = [this.saveHandles.saveFilePath.String, filename, '.png'];
    this.saveHandles.saveFileName.String = [filename, '.png'];
end
end
