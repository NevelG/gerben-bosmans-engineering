function pixelRejection(this, src, eventdata)

% check for valid input
if  isempty(this.editText.pixelRejection.String) || str2double(this.editText.pixelRejection.String)<1
    warndlg('this pixelRejection is not valid, interpetation was NaN or less than 1, resetting pixel treshold to 100 pixels (rounding will happen automatically)');
    this.editText.pixelRejection.String = '100';
    return % added a return so he does not execute the rejection if the person did not want 100 pixels rejected
end

% perform bwareaopen en show the result, bwareaopen removes all pixel
% clusters with a total area of less than the given number
area = round(str2double(this.editText.pixelRejection.String));
this.processedGhostedImage = bwareaopen(this.processedGhostedImage, area);
this.updateImage

end

