function analyseTracks(this, src, event)

if this.showAllBoxes
    this.resultTable.Data = cell.empty;
end

this.detectedBlobs = regionprops(this.processedGhostedImage, 'Centroid', 'BoundingBox','Perimeter','Area','MinorAxisLength','MajorAxisLength');

% get blob properties
if isempty(this.trainedModel)
typeRatio = zeros(numel(this.detectedBlobs),1);
for blobIdx = 1:numel(this.detectedBlobs)
    typeRatio(blobIdx,1) = (this.detectedBlobs(blobIdx).Area/this.detectedBlobs(blobIdx).Perimeter) * (this.detectedBlobs(blobIdx).MajorAxisLength  /  this.detectedBlobs(blobIdx).MinorAxisLength);
end

for blobIdx = 1:numel(this.detectedBlobs)
    % this is done to make sure the bounding box is
    % larger than the track and not exactly the track.
    boundingBoxCoords = this.detectedBlobs(blobIdx).BoundingBox;
    % this enlarges the bounding box coordinates but
    % not sure how i will go about it yet so for now
    % displaying it as exactly the area containing the
    % blob
    % boundingBoxCoords = boundingBoxCoords .* [0.95 0.95 1.2 1.2] + [0  0  (boundingBoxCoords(3)-boundingBoxCoords(3)*0.1)  (boundingBoxCoords(4)-boundingBoxCoords(4)*0.1)];
    try
        betaChance = this.mainGuiPointers.calculateProbability((typeRatio(blobIdx,1)+this.betaTypeOffset),...
            this.betaTypeMean, this.betaTypeDeviation, this.betaTypeInverseLambda);
        alphaChance = this.mainGuiPointers.calculateProbability((typeRatio(blobIdx,1)+this.alphaTypeOffset),...
            this.alphaTypeMean, this.alphaTypeDeviation, this.alphaTypeInverseLambda);
        isBeta = betaChance > alphaChance;
    catch
        % no support from statistics toolbox using a cuttof
        isBeta = typeRatio(blobIdx,1) < (this.betaTypeMean+2*this.betaTypeDeviation);
    end
    if isBeta % this means beta
        % if beta make bounding box red
        radiationTypeColor = [1 0 0];
        radiationTypeText = 'Beta';
    else  % this means alpha
        % if alpha make bounding box blue
        radiationTypeColor = [0 0 1];
        radiationTypeText = 'Alpha';
    end
    this.resultTable.Data{blobIdx,1}= radiationTypeText;
    if isBeta
        this.resultTable.Data{blobIdx,2}= betaChance/(betaChance+alphaChance);
    else
        this.resultTable.Data{blobIdx,2}= alphaChance/(alphaChance+betaChance);
    end
    this.resultTable.Data{blobIdx,3}= this.detectedBlobs(blobIdx).Area;
    this.resultTable.Data{blobIdx,4}= this.detectedBlobs(blobIdx).Perimeter;
    if this.showAllBoxes
        this.resultTable.Data{blobIdx,5}= true; %in eerste instantie is geen enkele blob geselecteerd
    end
    
    if this.resultTable.Data{blobIdx,5}
        % draw the rectangular bounding box outputs are
        % not used but will cause the rectangle to
        % disappear when the next frame is displayed
        selectedBoundingBox = rectangle(this.imageContainer,'Position',boundingBoxCoords,...
            'LineWidth', 1,...
            'EdgeColor', radiationTypeColor);
        % display a radiationtype annotation above the
        % bounding box
        selectedBoundingBox = text('Parent', this.imageContainer,...
            'Position',[boundingBoxCoords(1) boundingBoxCoords(2)],...
            'Color', radiationTypeColor,...
            'VerticalAlignment', 'bottom',...
            'FontSize', 8,...
            'String', radiationTypeText);
    end
    
    if this.showAllBoxes
        this.resultTable.Data{blobIdx,5}= false;
    end
end

else
    isAlpha = zeros(numel(this.detectedBlobs),1);
    for blobIdx = 1:numel(this.detectedBlobs)
        [isAlpha(blobIdx,1),posteriorProbability] = this.trainedModel.predictFcn([this.detectedBlobs(blobIdx).Centroid(1),this.detectedBlobs(blobIdx).Centroid(2),this.detectedBlobs(blobIdx).MinorAxisLength,...
            this.detectedBlobs(blobIdx).MajorAxisLength,...
            this.detectedBlobs(blobIdx).Perimeter,...
            this.detectedBlobs(blobIdx).Area]);
        
        if ~isAlpha % (not alpha means beta)
            % if beta make bounding box red
            radiationTypeColor = [1 0 0];
            radiationTypeText = 'Beta';
        else  % this means alpha
            % if alpha make bounding box blue
            radiationTypeColor = [0 0 1];
            radiationTypeText = 'Alpha';
        end
        
        this.resultTable.Data{blobIdx,1}= radiationTypeText;
        this.resultTable.Data{blobIdx,2}= posteriorProbability;
        this.resultTable.Data{blobIdx,3}= this.detectedBlobs(blobIdx).Area;
        this.resultTable.Data{blobIdx,4}= this.detectedBlobs(blobIdx).Perimeter;
        if this.showAllBoxes
            this.resultTable.Data{blobIdx,5}= true; %in eerste instantie is geen enkele blob geselecteerd
        end
        
        if this.resultTable.Data{blobIdx,5}
            % draw the rectangular bounding box outputs are
            % not used but will cause the rectangle to
            % disappear when the next frame is displayed
            selectedBoundingBox = rectangle(this.imageContainer,'Position',boundingBoxCoords,...
                'LineWidth', 1,...
                'EdgeColor', radiationTypeColor);
            % display a radiationtype annotation above the
            % bounding box
            selectedBoundingBox = text('Parent', this.imageContainer,...
                'Position',[boundingBoxCoords(1) boundingBoxCoords(2)],...
                'Color', radiationTypeColor,...
                'VerticalAlignment', 'bottom',...
                'FontSize', 8,...
                'String', radiationTypeText);
        end
        
        if this.showAllBoxes
            this.resultTable.Data{blobIdx,5}= false;
        end
    end
end

numberOfTableEntries = size(this.resultTable.Data,1);
numberOfBlobs = numel(this.detectedBlobs);
if numberOfBlobs < numberOfTableEntries
    this.resultTable.Data = this.resultTable.Data(1:numberOfBlobs,:);
end

if this.layoutSwapped
    if 0.4 > (0.050+0.025*numberOfBlobs)
        this.resultTable.Position = [0.500 0.55+(0.4-(0.050+0.025*numberOfBlobs)) 0.46 (0.050+0.025*numberOfBlobs)];
    else
        this.resultTable.Position = [0.5, 0.55, 0.46, 0.4];
    end
else
    if 0.55 > (0.050+0.025*numberOfBlobs)
        this.resultTable.Position = [0.5500 0.025+(0.55-(0.050+0.025*numberOfBlobs)) 0.42 (0.050+0.025*numberOfBlobs)];
    else
        this.resultTable.Position = [0.5500, 0.0250, 0.4200, 0.5500];
    end
    
end

