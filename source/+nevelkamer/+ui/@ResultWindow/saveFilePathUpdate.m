function saveFilePathUpdate(this, src, eventdata)

[pathName, fileName, extentionType]=  fileparts(this.saveHandles.saveFilePath.String);
if strcmpi(pathName,this.saveHandles.saveFilePath.String) || strcmpi(pathName,this.saveHandles.saveFilePath.String(1:end-1))
    [~,values] = fileattrib(pathName);
    if ~(values.UserWrite)
        warndlg('You do not have write permission to this directory, resetting your input to the current directory');
        this.saveHandles.saveFilePath.String = this.saveHandles.currentDirectory;
        return
    end
    if strcmpi(this.saveHandles.saveFilePath.String(end),'/') || strcmpi(this.saveHandles.saveFilePath.String(end),'\')
        this.saveHandles.saveFilePath.String = [pathName];
    else
        this.saveHandles.saveFilePath.String = [pathName, filesep];
    end
    this.saveHandles.exportPath.String = [this.saveHandles.saveFilePath.String,this.saveHandles.saveFileName.String];
elseif isdir(pathName)
    this.saveHandles.saveFilePath.String = [pathName, filesep];
    warndlg('the path entered is not a directory, we tried to guess what you wanted and put this as your input');
    [~,values] = fileattrib(pathName);
    if ~(values.UserWrite)
        warndlg('You do not have write permission to this directory, resetting your input to the current directory');
        this.saveHandles.saveFilePath.String = this.saveHandles.currentDirectory;
        return
    end
    this.saveHandles.exportPath.String = [this.saveHandles.saveFilePath.String,this.saveHandles.saveFileName.String];
else
    errordlg('the path entered is not a directory, if you do not know the correct directory path use the select directory button below for a small gui');
end
end
