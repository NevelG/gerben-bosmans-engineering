function applyImclose(this, src, eventdata)
if this.dropdown.shapeStrelImclose.String{this.dropdown.shapeStrelImclose.Value}== 'line'
    SizeLine=str2num(this.dropdown.filterSizeImclose.String{this.dropdown.filterSizeImclose.Value});
    this.processedGhostedImage = imclose(this.processedGhostedImage, strel(this.dropdown.shapeStrelImclose.String{this.dropdown.shapeStrelImclose.Value},SizeLine(1),SizeLine(2)));
    this.updateImage
else
    this.processedGhostedImage = imclose(this.processedGhostedImage, strel(this.dropdown.shapeStrelImclose.String{this.dropdown.shapeStrelImclose.Value}, str2num(this.dropdown.filterSizeImclose.String{this.dropdown.filterSizeImclose.Value})));
    this.updateImage
end

end

