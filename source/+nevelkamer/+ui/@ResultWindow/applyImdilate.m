function applyImdilate(this, src, eventdata)
if this.dropdown.shapeStrelImdilate.String{this.dropdown.shapeStrelImdilate.Value}== 'line'
    
    SizeLine= str2num(this.dropdown.filterSizeImdilate.String{this.dropdown.filterSizeImdilate.Value});
    this.processedGhostedImage = imdilate(this.processedGhostedImage, strel(this.dropdown.shapeStrelImdilate.String{this.dropdown.shapeStrelImdilate.Value}, SizeLine(1), SizeLine(2)));
    this.updateImage;
else
    this.processedGhostedImage = imdilate(this.processedGhostedImage, strel(this.dropdown.shapeStrelImdilate.String{this.dropdown.shapeStrelImdilate.Value}, str2num(this.dropdown.filterSizeImdilate.String{this.dropdown.filterSizeImdilate.Value})));
    this.updateImage;
    
end

end

