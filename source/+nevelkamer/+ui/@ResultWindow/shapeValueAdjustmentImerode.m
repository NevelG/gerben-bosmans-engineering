function shapeValueAdjustmentImerode(this, src, eventdata)
shapeStrel = this.dropdown.shapeStrelImerode.String{this.dropdown.shapeStrelImerode.Value};
switch shapeStrel
    case 'disk'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'3','5','7','10','15','20', '30', '40', '50', '60', '80', '100'};
    case 'diamond'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'3','5','7','10','15','20', '30', '40', '50', '60', '80', '100'};
    case 'line'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'[3 0]','[3 90]','[5 0]','[5 45]','[5 90]','[7 0]','[7 45]','[7 90]','[10 0]','[10 45]','[10 90]', '[20 0]','[20 45]','[20 90]',...
            '[30 0]','[30 45]','[30 90]','[50 0]','[50 45]','[50 90]'};
    case 'octagon'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'3','6','9','12','15','18','21','30', '39', '60', '90'};
    case 'rectangle'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'[2 3]','[3 2]','[4 6]','[6 4]','[6 8]','[8 6]','[8 10]','[10 8]', '[10 15]', '[15 10]', '[10 20]', '[20 10]',...
            '[10 30]', '[30 10]', '[20 50]', '[50 20]'};
    case 'square'
        this.dropdown.filterSizeImrode.Value = 1;
        this.dropdown.filterSizeImerode.String = {'3','5','7','10','15','20', '30', '40', '50', '60', '80', '100'};
end
end

