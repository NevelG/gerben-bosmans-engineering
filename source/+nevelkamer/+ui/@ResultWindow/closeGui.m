function closeGui(this,src,eventdata)
% create a popup window
selection = questdlg('Are you sure you wish to quit our fabulous result window?',...
    'Closing',...
    'Quit','Cancel','Save and Quit','Quit');
% give the user options to chose what he wants to do before confirming
switch selection
    case 'Quit'
        delete(this.figureHandle);
    case 'Cancel'
        return
    case 'Save and Quit'
        saveResults;
        delete(this.figureHandle);
end

end

