function selectTrack(this, src, eventdata) 
% HIER werkt iets niet en ik vind ni wa :-(
    % updates the filters used in the current video and also
    % shows which filters will be used when processing the data
    this.interruptUserInteraction = false;
    
    if ~this.interruptUserInteraction
    axisLimits = this.imageContainer.Position;
    figureLimits = this.figureHandle.Position;
    this.axisPositions.xStart = axisLimits(1)*figureLimits(3) + figureLimits(1);
    this.axisPositions.xEnd = (axisLimits(1)+axisLimits(3))*figureLimits(3) + figureLimits(1);
    this.axisPositions.yStart = axisLimits(2)*figureLimits(4) + figureLimits(2);
    this.axisPositions.yEnd = (axisLimits(2)+axisLimits(4))*figureLimits(4) + figureLimits(2);
    this.figureHandle.WindowButtonMotionFcn = @this.changePointer;
    this.imageContainer.ButtonDownFcn = @this.getCoordinates;
    
    numberOfChildren = numel(this.imageContainer.Children);
    if (numberOfChildren == 1)
        this.imageContainer.Children.ButtonDownFcn = @this.getCoordinates;
    elseif (numberOfChildren == 0)
        errordlg('You cant select a track without an image')
    else
        for childIdx = 1:numberOfChildren
           this.imageContainer.Children(childIdx).ButtonDownFcn = @this.getCoordinates; 
        end
    end
    
    this.figureHandle.ButtonDownFcn = @this.getCoordinates;

    else
        return
    end
end





