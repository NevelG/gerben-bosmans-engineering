function applyImerode(this, src, eventdata)
if this.dropdown.shapeStrelImerode.String{this.dropdown.shapeStrelImerode.Value}== 'line'
    SizeLine= str2num(this.dropdown.filterSizeImerode.String{this.dropdown.filterSizeImerode.Value});
    this.processedGhostedImage = imerode(this.processedGhostedImage, strel(this.dropdown.shapeStrelImerode.String{this.dropdown.shapeStrelImerode.Value},SizeLine(1), SizeLine(2)));
    this.updateImage;
else
    this.processedGhostedImage = imerode(this.processedGhostedImage, strel(this.dropdown.shapeStrelImerode.String{this.dropdown.shapeStrelImerode.Value}, str2num(this.dropdown.filterSizeImerode.String{this.dropdown.filterSizeImerode.Value})));
    this.updateImage;
end
end

