function changePointer(this, src, eventdata)
pointerHandle = get(0,'PointerLocation');
% nested the if statements for readability as constant && made it
% unreadable on a laptop screen
if pointerHandle(1)>this.axisPositions.xStart
    if pointerHandle(1)<this.axisPositions.xEnd
        if pointerHandle(2)>this.axisPositions.yStart
            if pointerHandle(2)<this.axisPositions.yEnd
                if this.imageContainer.CurrentPoint(1,1) < this.mainGuiPointers.videoHandle.resolution(1) && this.imageContainer.CurrentPoint(1,1) > 0
                    if this.imageContainer.CurrentPoint(1,2) < this.mainGuiPointers.videoHandle.resolution(2) && this.imageContainer.CurrentPoint(1,2) > 0
                        set(this.figureHandle,'Pointer','crosshair')
                    else
                        set(this.figureHandle,'Pointer','arrow')
                    end
                else
                    set(this.figureHandle,'Pointer','arrow')
                end
            else
                set(this.figureHandle,'Pointer','arrow')
            end
        else
            set(this.figureHandle,'Pointer','arrow')
        end
    else
        set(this.figureHandle,'Pointer','arrow')
    end
else
    set(this.figureHandle,'Pointer','arrow')
end

end
