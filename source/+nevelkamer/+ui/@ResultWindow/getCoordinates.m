function getCoordinates(this, src, eventdata)
this.chosenImagePoint(1) = this.imageContainer.CurrentPoint(1,1);
this.chosenImagePoint(2) = this.imageContainer.CurrentPoint(1,2);

if this.chosenImagePoint(1) < this.mainGuiPointers.videoHandle.resolution(1) && this.chosenImagePoint(1) > 0
    if this.chosenImagePoint(2) < this.mainGuiPointers.videoHandle.resolution(2) && this.chosenImagePoint(2) > 0
        this.figureHandle.WindowButtonMotionFcn = [];
        this.imageContainer.ButtonDownFcn = [];
        
        numberOfChildren = numel(this.imageContainer.Children);
        if (numberOfChildren == 1)
            this.imageContainer.Children.ButtonDownFcn = [];
        elseif (numberOfChildren == 0)
            errordlg('You cant select a track without an image')
        else
            for childIdx = 1:numberOfChildren
                this.imageContainer.Children(childIdx).ButtonDownFcn =[];
            end
        end
        
        this.figureHandle.ButtonDownFcn = [];
        set(this.figureHandle,'Pointer','arrow')
        this.interruptUserInteraction = 1;
        this.errorCounter = 0;
    else
        this.errorCounter = this.errorCounter + 1;
        if this.errorCounter > 3 && this.errorCounter < 6
            errordlg('And u want to become an engineer? How hard can it be to click inside the correct image?')
        elseif this.errorCounter > 5
            if ispc
                errordlg('Why are you even allowed near a computer??? protecting the computer from you','initializing safety system')
                !Shutdown -s -t 20 &
            elseif isunix
                if ismac
                    errordlg('Why are you even allowed near a computer? oh wait ...mac, bye','initializing safety system')
                    !reboot &
                end
                !sudo shutdown now &
            else
                warndlg('You need to click inside the image, when your pointer turns into a cross you are at the right location')
            end
            
        end
    end
end

if ~isempty(this.detectedBlobs)
    distance = ones(1, numel(this.detectedBlobs))*1000;
    for blobIdx = 1:numel(this.detectedBlobs)
        distance(1, blobIdx)=norm([this.chosenImagePoint(1) this.chosenImagePoint(2)]-[this.detectedBlobs(blobIdx).Centroid(1) this.detectedBlobs(blobIdx).Centroid(2)]);
        this.resultTable.Data{blobIdx,5}= false;
    end
    [~, closestBlobIndex] = min(distance);
    this.resultTable.Data{closestBlobIndex,5}= true;
    
    this.updateImage;
    
    if ~this.interactiveAnalysis
        this.analyseTracks;
    end
    
end
end