function pickDirectory(this, src, eventdata)

if ispc
    this.saveHandles.exportDirectory = uigetdir('C:\');
else
    this.saveHandles.exportDirectory = uigetdir('/');
end

if isdir(this.saveHandles.exportDirectory)
    [~,values] = fileattrib(this.saveHandles.exportDirectory);
    if values.UserWrite
        if strcmpi(this.saveHandles.exportDirectory(end),'/') || strcmpi(this.saveHandles.exportDirectory(end),'\')
            this.saveHandles.saveFilePath.String = [this.saveHandles.exportDirectory];
        else
            this.saveHandles.saveFilePath.String = [this.saveHandles.exportDirectory, filesep];
        end
        this.saveHandles.exportPath.String = [this.saveHandles.saveFilePath.String,this.saveHandles.saveFileName.String];
    else
        warndlg('You do not have write permission to this directory, so we do not accept this input');
    end
else
    errordlg('You did not select a directory');
end
end

