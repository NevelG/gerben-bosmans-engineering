function calculateMagneticFieldRoi(this, src ,eventdata)
this.magnetPosition.theta = linspace(0, 2*pi, 300);
this.magnetPosition.rho = ones(1, 300).*this.magnetPosition.radius;
[x, y] = pol2cart(this.magnetPosition.theta, this.magnetPosition.rho);
x = x + this.magnetPosition.center(2);
y = y + this.magnetPosition.center(1);
currentFrame = this.videoHandle.currentFrameNumber;

this.imageMagnetPosition = roipoly(this.videoHandle.nextFrame(currentFrame), x, y);
this.setMagnetOrientation;
end
