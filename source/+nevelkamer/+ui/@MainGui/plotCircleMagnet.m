function plotCircleMagnet(this, src ,eventdata)
this.magnetPosition.theta = linspace(0, 2*pi, 50);
this.magnetPosition.rho = ones(1, 50).* this.magnetPosition.radius;
[x, y] = pol2cart(this.magnetPosition.theta, this.magnetPosition.rho);
x = x+this.magnetPosition.center(1); y = y+this.magnetPosition.center(2);
set(this.magnetPosition.circleHandle,'xdata',y,'ydata',x)
end

