function openResultWindow(this, src ,eventdata)

% opens a second GUI window that simply displays the
% results and allows selection of tracks etc
this.resultHandle = nevelkamer.ui.ResultWindow(this);

end
