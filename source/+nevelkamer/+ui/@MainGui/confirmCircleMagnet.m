function confirmCircleMagnet(this, src ,eventdata)
enteredKeyID = get(this.figureHandle, 'CurrentKey');
if strcmpi(enteredKeyID,'return')
    this.magnetPosition.dragging = false;
    this.figureHandle.WindowButtonDownFcn = [];
    this.figureHandle.WindowButtonUpFcn = [];
    this.figureHandle.WindowButtonMotionFcn = [];
    this.figureHandle.KeyPressFcn = [];
    this.staticText.magnetInstructions.Visible = 'off';
    hold(this.axisRight, 'off');
    this.magnetPosition.interruptValue = true;
    this.staticText.instructionsMagnet.Visible = 'off';
    this.calculateMagneticFieldRoi;
end
end
