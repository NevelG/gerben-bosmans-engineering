function updateBackgroundTreshold(this, src ,eventdata)
if ~isempty(this.backgroundModel)
    normalizedTresholdValue = str2double(this.editText.normalizedThreshold.String);
    if normalizedTresholdValue  < 0 || isnan(normalizedTresholdValue ) || normalizedTresholdValue > 1
        this.buttons.togglePlay = false;
        this.editText.normalizedThreshold.String = '0.35';
        normalizedTresholdValue = 0.35;
        errordlg('pixeltreshold is not valid, interpetation was NaN or not between 0 and 1, we took the liberty of setting it back to 0.35 (rounding will happen automatically)')
    end
    
    this.backgroundModel.treshold = normalizedTresholdValue;
end

end

