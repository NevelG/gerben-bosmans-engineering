function adjustValueToShape(this, src ,eventdata)

shapeStrel = this.dropdown.shapeStrel.String{this.dropdown.shapeStrel.Value};
switch shapeStrel
    case 'disk'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'3','5','7','10','15','20'};
    case 'diamond'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'3','5','7','10','15','20'};
    case 'line'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'[3 0]','[3 90]','[5 0]','[5 45]','[5 90]','[7 0]','[7 45]','[7 90]','[10 0]','[10 45]','[10 90]'};
    case 'octagon'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'3','6','9','12','15','18'};
    case 'rectangle'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'[2 3]','[3 2]','[4 6]','[6 4]','[6 8]','[8 6]','[8 10]','[10 8]'};
    case 'square'
        this.dropdown.filterSize.Value = 1;
        this.dropdown.filterSize.String = {'3','5','7','10','15','20'};
end

end