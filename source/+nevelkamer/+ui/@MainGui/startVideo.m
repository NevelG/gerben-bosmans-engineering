function startVideo(this, src ,eventdata)
% Starts the loop of image
this.togglePlay = true;
currentFrame = this.videoHandle.currentFrameNumber;
frameTime = (1/this.videoHandle.video.frameRate);

% if we want to show track recognition we will play the video
% with a delay to be able to perform the ghosting
if this.checkbox.trackRecognition.Value && strcmp(this.liveTrackingHandle.mode,'fast')
    tic;
    % this will be used to initialize the live tracking matrix
    liveTrackingFrame = 1;
    % check if the frame number makes sence
    numberOfFrames = round(str2double(this.editText.recognitionNumberOfFrames.String));
    
    middleFrame = round(numberOfFrames/2);
    if this.videoHandle.currentFrameNumber < (middleFrame +1)
        trash = this.videoHandle.nextFrame(middleFrame +1); %#ok<NASGU> we know we won't use the output but tilda is not allowed
        % this is a recursive function to make sure when the
        % video starts it does not ghost from frames that don't
        % exist
        this.startVideo
    else
        frameShown = this.videoHandle.currentFrameNumber - middleFrame;
        while this.togglePlay
            % this protects causes the video to loop back to the
            % beginning after it finished
            if currentFrame >= (this.videoHandle.numberOfFrames-round(numberOfFrames/2))
                currentFrame = middleFrame+1;
                this.liveTrackingHandle = [];
                this.checkbox.trackRecognition.Value = 0;
                this.togglePlay = false;
                break
            end
            outputImage = this.videoHandle.nextFrame(currentFrame);
            
            if liveTrackingFrame > numberOfFrames
                
                tempImageMatrix = this.liveTrackingHandle.imageMatrix;
                tempTrackMatrix = this.liveTrackingHandle.trackMatrix;
                % add new track to the end then cut of oldest
                % frame
                tempImageMatrix(:,:,:,end+1) = outputImage;
                tempTrackMatrix(:,:,end+1) = this.backgroundModel.showTracks(outputImage);
                
                this.liveTrackingHandle.imageMatrix = tempImageMatrix(:,:,:,2:end);
                this.liveTrackingHandle.trackMatrix = tempTrackMatrix(:,:,2:end);
               
                currentFrame = currentFrame +1;
                frameShown = frameShown + 1;
                this.updateTrackedImageRight(middleFrame);
                this.updateTrackedImageLeft(middleFrame);
                this.staticText.frameNumber.String = sprintf('%d',frameShown);
                this.slider.frames.Value = frameShown;
                processingTime = toc;
                timeToPause = frameTime - processingTime;
                if timeToPause < 0.0333 || timeToPause > 0
                    pause(timeToPause);
                else
                    pause(0.0333);
                end
                drawnow; % this is a very important part of the code as it causes the GUI to check for callbacks for each update of the figure
            else
                this.liveTrackingHandle.imageMatrix(:,:,:,liveTrackingFrame) = outputImage;
                this.liveTrackingHandle.trackMatrix(:,:,liveTrackingFrame) = this.backgroundModel.showTracks(outputImage);
                liveTrackingFrame = liveTrackingFrame + 1;
                % this is to stop the initialized tic incase we
                % do not have the correct frame yet;
                dummyTimeVariable = toc;
            end
        end
    end
    
    % if live tracking is not applied we use the functions
    % updateRight and updateLeft and do not apply a delay
else
    while this.togglePlay
        tic;
        if currentFrame == this.videoHandle.numberOfFrames
            currentFrame = 1;
        end
        % grab current frame
        outputImage = this.videoHandle.nextFrame(currentFrame);
        currentFrame = currentFrame +1;
        this.updateRightImage(outputImage);
        this.updateLeftImage(outputImage);
        % update frame number on text
        this.staticText.frameNumber.String = sprintf('%d',this.videoHandle.currentFrameNumber);
        this.slider.frames.Value = this.videoHandle.currentFrameNumber;
        % check how long all processing took and pause accordingly
        processingTime = toc;
        timeToPause = frameTime - processingTime;
        if timeToPause < 0.0333 || timeToPause > 0
            pause(timeToPause)
        end
        drawnow; % this is a very important part of the code as it causes the GUI to check for callbacks for each update of the figure
    end
end

end