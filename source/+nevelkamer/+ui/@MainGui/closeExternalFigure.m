function closeExternalFigure(this, src, eventdata)
    delete(this.externalFigure);
    this.externalFigure = [];
end