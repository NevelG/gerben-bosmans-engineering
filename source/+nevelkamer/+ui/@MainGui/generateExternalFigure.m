function generateExternalFigure(this, src, eventdata)
% Get information about number of connected screens and their
% resolution
screenInfo = groot;

% prioritize the second screen for displaying the GUI
screenIdx = min(size(screenInfo.MonitorPositions,1));

% this sets the size of the gui when it gets minimized for the
% first time, which makes the resizing a bit easier than a gui
% of the same size as your screen
externalPositions = [screenInfo.MonitorPositions(screenIdx, 1) screenInfo.MonitorPositions(screenIdx, 2) screenInfo.MonitorPositions(screenIdx, 3)*0.8 screenInfo.MonitorPositions(screenIdx, 4)*0.8];

this.externalFigure = figure('NumberTitle','off',...
    'Color', [0.9 0.9 0.9], ...
    'Name','External Figure - Project TNE2');

currentFrameNumber = this.videoHandle.currentFrameNumber;
imshow(this.videoHandle.nextFrame(currentFrameNumber));
set(this.externalFigure, 'Position', externalPositions);
frame_h = get(handle(this.externalFigure),'JavaFrame');
set(frame_h,'Maximized',1);

set(this.externalFigure,'CloseRequestFcn',@this.closeExternalFigure)
end