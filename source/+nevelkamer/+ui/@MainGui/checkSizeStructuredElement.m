function checkSizeStrel(this, src, eventdata)

sizeStructuredElement = round(str2double(this.editText.sizeStructuredElement.String));
if sizeStructuredElement < 1 || isnan(sizeStructuredElement)
    this.buttons.togglePlay = 0;
    errordlg('size of structered element is not valid, interpetation was NaN or less than 1, resetting size structured element to 40 pixels (rounding will happen automatically)')
    this.editText.sizeStructuredElement.String = '40';
end

end

