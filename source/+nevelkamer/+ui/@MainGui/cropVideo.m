function cropVideo(this, src ,eventdata)

if isfield(this,'liveTrackingHandle') && this.checkbox.trackRecognition.Value
    this.pauseVideo;
    drawnow;
    this.liveTrackingHandle.trackMatrix = [];
    this.liveTrackingHandle.imageMatrix = [];
    this.startVideo
end

this.cropField.rectangle = imrect(this.axisLeft);
if ~isempty(this.videoHandle.cropCoordinates)
    columnOffset = this.videoHandle.cropCoordinates(1)-1;
    rowOffset = this.videoHandle.cropCoordinates(2)-1;
else
    columnOffset=0;
    rowOffset=0;
    % positions=[xmin ymin xmax ymax];
    % crop all images based on this region of interest to reduce computation
    % time and data size automatically detected the first region
end
positions = this.cropField.rectangle.getPosition;
this.videoHandle.cropCoordinates =...
    [columnOffset+ceil(positions(1))...
    rowOffset+ceil(positions(2))...
    floor(columnOffset+positions(1)+positions(3))...
    floor(rowOffset+positions(2)+positions(4))];
delete(this.cropField.rectangle);
updateLeftImage(this,this.videoHandle.nextFrame(this.videoHandle.currentFrameNumber));
% positions=[xmin ymin xmax ymax];
% crop all images based on this region of interest to reduce computation
% time and data size automatically detected the first region

end

