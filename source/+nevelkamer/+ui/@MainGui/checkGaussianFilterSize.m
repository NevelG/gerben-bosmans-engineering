function checkGaussianFilterSize(this, src, eventdata)

gaussianSize = round(str2double(this.editText.filterSizeBox.String));
isOdd = bitget(gaussianSize,1);

if isnan(gaussianSize) || isempty(this.editText.filterSizeBox.String)
    errordlg('Filter size is not a positive integer, filter size needs to be an odd positive integer (rounding to an integer is done automatically when possible)');
    this.togglePlay = 0;
    this.editText.recognitionNumberOfFrames.String = '7';
elseif ~isOdd
    warndlg('Filter size needs to be an odd number,adding 1 to your given filter size and rounding');
    gaussianSize = gaussianSize + 1;
    this.editText.filterSizeBox.String = num2str(gaussianSize);
end

end