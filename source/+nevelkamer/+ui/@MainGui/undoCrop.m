function undoCrop(this, src ,eventdata)

if isfield(this,'liveTrackingHandle') && this.checkbox.trackRecognition.Value
    this.pauseVideo;
    drawnow;
    this.liveTrackingHandle.trackMatrix = [];
    this.liveTrackingHandle.imageMatrix = [];
    this.startVideo
end

this.videoHandle.cropCoordinates = [];
updateLeftImage(this, this.videoHandle.nextFrame(this.videoHandle.currentFrameNumber));

end