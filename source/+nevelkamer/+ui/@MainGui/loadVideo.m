function loadVideo(this, src ,eventdata)
% popup window for browsing for the video file
[ fileName,pathName ] = uigetfile({'*.*'},'Pick a video file');

if(pathName == 0)
    return;
end
pause(1)

inputVideoFile = [pathName, fileName];
this.filePathHandle = nevelkamer.io.VideoFilePath(inputVideoFile);

if ~this.filePathHandle.isVideo || ~this.filePathHandle.readPermission
    if ~this.filePathHandle.isVideo
    errordlg('The file you tried to load is either not a video or an unsupported format, try again');
    else
       errordlg('You do not have read permission on the video');
    end
    
    return
end

this.videoHandle = nevelkamer.data.VideoData(this.filePathHandle);
% set processing mode for videoHandle class
this.videoHandle.processingMode = this.dropdown.processingType.String{this.dropdown.processingType.Value};

switch this.videoHandle.processingMode
    case 'CPU Accuracy'
        updatedFrame = uint8(this.videoHandle.nextFrame(1));
    otherwise
        updatedFrame = this.videoHandle.nextFrame(1);
end

updateLeftImage(this, updatedFrame);
% change texts
this.staticText.filePrint.String = this.filePathHandle.filePath;
this.staticText.frameNumber.String = '1';
this.staticText.totalFrames.String = sprintf('%d', this.videoHandle.numberOfFrames);
% update background button so it can be pressed
this.buttons.setBackground.Enable = 'on';

% change slidersize
this.slider.frames.Max = this.videoHandle.numberOfFrames; % adapt the maximum number of frames placed in the slider
if this.videoHandle.numberOfFrames < 1000 % with more than 1000 frames a fraction is taken and with less 20 frames is taken as the gross change
    this.slider.frames.SliderStep = [1/(this.videoHandle.numberOfFrames) 20/(this.videoHandle.numberOfFrames)];
else
    this.slider.frames.SliderStep = [1/(this.videoHandle.numberOfFrames) 0.04];
end

this.slider.brightness.Max =  1;
this.slider.brightness.Min =  0.51;
this.slider.brightness.SliderStep = [0.01 0.1];
% indicate that this is frame one and set slider to begin
% position
this.slider.frames.Value = 1;
% remove previous video data:
this.trainedModel = [];
this.liveTrackingHandle = [];
this.imageMagnetPosition = [];
this.backgroundModelInfo = [];
this.backgroundModel = nevelkamer.analysis.DetectTracks.empty;
this.checkbox.trackRecognition.Value = 0;
this.dropdown.displayType.Value = 1;
end
