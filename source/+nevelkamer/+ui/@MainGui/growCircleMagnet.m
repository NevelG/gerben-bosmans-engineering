function growCircleMagnet(this, src ,eventdata)

if (this.magnetPosition.dragging)
    coord = get(this.axisRight,'CurrentPoint');
    coord = [coord(1,2),coord(1,1)];
    % calculate distance between current coord and center
    this.magnetPosition.radius = round(...
        sqrt((coord(1)-this.magnetPosition.center(1))^2 +...
        (coord(2)-this.magnetPosition.center(2))^2));
    this.plotCircleMagnet;
end

end

