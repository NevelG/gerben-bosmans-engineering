function updateTrackedImageLeft(this, middleFrame)

imadjustValue = this.slider.brightness.Value; %choose the brightness value
% adapt brightness with imadjust and output with imshow based on array type
inputImage = this.liveTrackingHandle.imageMatrix(:,:,:,middleFrame);
switch this.videoHandle.processingMode
    case 'GPU Speed'
        outputImage = imadjust(inputImage,[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',this.axisLeft);
    case 'CPU Accuracy'
        outputImage = imadjust(uint8(inputImage),[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',this.axisLeft);
    case 'CPU Speed'
        outputImage = imadjust(inputImage,[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',this.axisLeft);
end

end