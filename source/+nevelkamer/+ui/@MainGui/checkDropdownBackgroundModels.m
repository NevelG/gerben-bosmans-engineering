function checkDropdownBackgroundModels(this, src, eventdata)

idx = this.popupHandle.dropdownBackgroundModels.Value;
choiceBackgroundModel = this.popupHandle.dropdownBackgroundModels.String;
this.backgroundModelInfo.type = char(choiceBackgroundModel(idx,:));

if strcmpi(char(choiceBackgroundModel(idx,:)),'Averaged Frames')
    this.popupHandle.values.editTextLastFrame.Enable = 'on';
elseif strcmpi(char(choiceBackgroundModel(idx,:)),'Median Background Model')
    this.popupHandle.values.editTextLastFrame.Enable = 'on';
elseif strcmpi(char(choiceBackgroundModel(idx,:)),'Gaussian Mixture Model')
    this.popupHandle.values.editTextLastFrame.Enable = 'on';
else
    this.popupHandle.values.editTextLastFrame.Enable = 'off';
end


end

