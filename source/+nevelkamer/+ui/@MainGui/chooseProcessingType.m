function chooseProcessingType(this, src ,eventdata)
askConfirm = questdlg('Changing processing mode will reset background model, are u sure you wish to change',...
    'Changing processing mode',...
    'Yes','No','Yes');

switch askConfirm
    case 'Yes'
        this.processingMode = this.dropdown.processingType.Value;
    case 'No'
        this.dropdown.processingType.Value = this.processingMode;
        return
end

processingType = this.dropdown.processingType.String{this.dropdown.processingType.Value};
this.dropdown.displayType.Value = 1;
this.backgroundModelInfo = [];
this.backgroundModel = nevelkamer.analysis.DetectTracks.empty;
this.checkbox.trackRecognition.Value = 0;
this.dropdown.displayType.Value = 1;
try
    if ~isempty(this.videoHandle)
        this.videoHandle.processingMode = processingType;
    else
        warndlg('No video was detected when switching processing type, i you did alreadly load in a video please reload it')
    end
catch
    warndlg('No video was detected when switching processing type, i you did alreadly load in a video please reload it')
end

if this.hasNvidiaGPU
    switch processingType
        case 'GPU Speed'
            this.dropdown.processingType.TooltipString = 'GPU speed uses CUDA acceleration to but all processing is done using logicals and 8 bit unsigned integers slightly reducing accuracy';
        case 'CPU Accuracy'
            this.dropdown.processingType.TooltipString = 'This method uses the most accurate image processing as all matrices are processed at double precision but this comes at the cost of speed';
        case 'CPU Speed'
            this.dropdown.processingType.TooltipString = 'This runs everything on the CPU but uses 8 bit unsigned integers to speed things up, but u have a Nvidia GPU so GPU speed is adviced over this mode';
    end
else
    switch processingType
        case 'CPU Speed'
            this.dropdown.processingType.TooltipString = 'All processing is done using 8 bit unsigned integers to speed up processing, GPU acceleration was not possbile as no CUDA supporting GPU was detected';
        case 'CPU accuracy'
            this.dropdown.processingType.TooltipString = 'This method uses the most accurate image processing as all matrices are processed at double precision but this comes at the cost of speed';
    end
end

end