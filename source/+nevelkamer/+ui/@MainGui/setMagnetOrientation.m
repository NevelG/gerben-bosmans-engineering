function setMagnetOrientation(this, src, eventdata)
selection = questdlg('How was the magnet oriented?',...
    'Setting Magnet Orientation',...
    'North Pole Up','South Pole Up','North Pole Up');

switch selection
    case 'North Pole Up'
        this.magnetOrientation = 'North';
    case 'South Pole Up'
        this.magnetOrientation = 'South';
end
end

