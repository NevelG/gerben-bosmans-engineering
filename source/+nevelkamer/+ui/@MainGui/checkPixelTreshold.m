function checkPixelTreshold(this, src, eventdata)

pixelTresholdValue = round(str2double(this.editText.pixelThreshold.String));
if pixelTresholdValue  < 1 || isnan(pixelTresholdValue )
    this.buttons.togglePlay = 0;
    warndlg('pixeltreshold is not valid, interpetation was NaN or less than 1, resetting pixel treshold to 100 pixels (rounding will happen automatically)')
    this.editText.pixelThreshold.String = '100';
end

end

