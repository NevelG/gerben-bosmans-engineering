function checkMemoryForTracking(this, src, eventdata)
% this function checks if your computer can handle the live
% tracking with the fast method, if not a slower method is
% used but it will not surpas the 1-2 frames per second due to
% constant changing of frame number
% to make sure we can handle the processing we allocate a
% double array as check then clear it and replace it with a
% logical array which will be used for the tracking itself.
wasRunning = false;
if this.togglePlay
    wasRunning = true;
end

this.togglePlay = false;
drawnow;
if this.checkbox.trackRecognition.Value
    drawnow;
    numberOfFrames = round(str2double(this.editText.recognitionNumberOfFrames.String));
    
    if ~isempty(this.videoHandle)
        sizeOfTheTrackingArray = [this.videoHandle.resolution 3 numberOfFrames];
    else
        errordlg('You cannot do live tracking if you have not even loaded in a video');
        this.checkbox.trackRecognition.Value = 0;
        return
    end
    
    if isempty(this.backgroundModel)
        errordlg('You selected live tracking without building a background model first, this is ofcourse not possible. We took the liberty of unchecking the live tracking box for you');
        this.checkbox.trackRecognition.Value = 0;
        return
    end
    
    try
        dummyMatrix1= rand(sizeOfTheTrackingArray);
        dummyMatrix2 = rand(sizeOfTheTrackingArray);
        dummyMatrix3 = rand(sizeOfTheTrackingArray);
        dummyMatrix4 = rand(sizeOfTheTrackingArray);
        calculatedMean = mean(dummyMatrix3,4);
        this.liveTrackingHandle.mode = 'fast';
    catch
        this.liveTrackingHandle.mode = 'memory sparing';
        warndlg('Your computer cannot handle fast ghosting over this amount of frames, consider using less frames if you still want to use the fast version of ghosting');
    end
    
else
    this.liveTrackingHandle = [];
end

if wasRunning
   this.startVideo 
end

end