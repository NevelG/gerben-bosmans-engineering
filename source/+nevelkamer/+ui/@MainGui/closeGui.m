function closeGui(this, src ,eventdata)

selection = questdlg('Are you sure you wish to quit our amazing program without saving?',...
    'Closing',...
    'Quit','Cancel','Save and Quit','Quit');

switch selection
    case 'Quit'
        delete(this.figureHandle)
    case 'Cancel'
        return
    case 'Save and Quit'
        % will replace this by just saving a text file
        % with comments and a video file but for now
        % this is just a example of how quit protection
        % can be done
        saveResults;
        delete(this.figureHandle)
end
end
