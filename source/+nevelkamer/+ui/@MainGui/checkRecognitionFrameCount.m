function checkRecognitionFrameCount(this)

numberOfFrames = round(str2double(this.editText.recognitionNumberOfFrames.String));
if isnan(numberOfFrames) || numberOfFrames < 1
    warndlg('Number of frames is not a positive integer, changing the input back to 10 frames(rounding to an integer is done automatically when possible)');
    this.buttons.togglePlay = 0;
    this.editText.recognitionNumberOfFrames.String = '10';
elseif numberOfFrames > 30
    warndlg('Making the live recognition perform ghosting over 30 or more frames is not adviced unless you used a highspeed camera, 10 frames is a good number to aim for if you used a regular 30 fps camera');
end

end
