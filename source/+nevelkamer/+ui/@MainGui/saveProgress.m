function saveProgress(this, src ,eventdata)

% saves the GUI test handles in a .mat file
timeStampString = datestr(now, 'ddmmmyyyy_HHMMSS');
fileNameSuggestion = ['GuiState_',timeStampString,'.mat'];
[fileName, filePath, ~] = uiputfile(fileNameSuggestion);
fullPath = [filePath, fileName];
previousGuiState = this; %#ok<NASGU> it is used when trying to save
save(fullPath, 'previousGuiState');

end
