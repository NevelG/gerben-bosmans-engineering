function closeMachineLearningPopup(this, src ,eventdata)

% the currentframe number stuff makes sure the background model does not fastforward
% the video when u press play after the background calculation
modelType = this.popupHandle.dropdownTrainingModels.String{this.popupHandle.dropdownTrainingModels.Value};
if strcmpi(modelType, 'Support Vector Machine')
    subType = 'linear';
else
    subType = 100;
end
trainingMatrix = evalin('base', 'trainingMatrix');
[trainedClassifier, validationAccuracy] = this.backgroundModel.trainRecognitionModel(...
    trainingMatrix,...
    modelType,...
    subType);

this.trainedModel = trainedClassifier;
accuracyString = sprintf('The accuracy of your trained model is %0.2f on the training data set', validationAccuracy);
msgbox(accuracyString, 'Accuracy of the classifier')
close(this.popupHandle.machineLearning);
this.popupHandle = [];

end