function checkSigmaInput(this, src, eventdata)

sigmaValue = str2double(this.editText.sigmaValueBox.String);

if isnan(sigmaValue) || isempty(this.editText.sigmaValueBox.String)
    errordlg('Sigma is not a number, resetting it to 5');
    this.togglePlay = 0;
    this.editText.recognitionNumberOfFrames.String = '5';
end

end

