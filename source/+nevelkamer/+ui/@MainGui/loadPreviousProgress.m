function loadPreviousProgress(this, src ,eventdata)
% loads a .mat file with gui handles and sets this = .mat
% file content to update it completely
fileName = (uigetfile('*.mat'));
this = load(fileName,'previousGuiState');
end