function updateSliderFrames(this, src ,eventdata)
% dit stukje zorgt ervoor dat je een video die op play staat automatisch pauseerd
this.togglePlay = false;

% this takes care of the track recognition incase the slider is
% used
newFrameNumber = round(this.slider.frames.Value); % retrieve the value of the slider
updatedFrame = this.videoHandle.nextFrame(newFrameNumber);  % in VideoData class go to the frame that corresponds with the new frame number
this.staticText.frameNumber.String = sprintf('%d',newFrameNumber); % change the frameNumber in the GUI

if ~isempty(this.liveTrackingHandle)
    if strcmp(this.liveTrackingHandle.mode, 'fast') && this.checkbox.trackRecognition.Value
        numberOfFrames = round(str2double(this.editText.recognitionNumberOfFrames.String));
        if isnan(numberOfFrames) || numberOfFrames < 1
            warndlg('Number of frames and/or frame skipping is not a natural number, min value for frames = 1 and skipping = 0, rounding is done automatically');
            this.buttons.togglePlay = 0;
        end
    end
end
% effectieve slider functie
this.liveTrackingHandle.mode = 'memory sparing';

updateRightImage(this, updatedFrame);
updateLeftImage(this, updatedFrame);

end