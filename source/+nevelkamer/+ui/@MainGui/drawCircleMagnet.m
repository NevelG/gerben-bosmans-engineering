function drawCircleMagnet(this, src ,eventdata)
% updates the filters used in the current video and also
% shows which filters will be used when processing the data
this.magnetPosition.radius = [];
this.magnetPosition.interruptValue = 0;
this.magnetPosition.center = 0;

this.figureHandle.WindowButtonDownFcn = @this.getCenterMagnet;
this.figureHandle.WindowButtonUpFcn = @this.stopPlottingMagnet;
this.figureHandle.WindowButtonMotionFcn = @this.growCircleMagnet;
this.figureHandle.KeyPressFcn = @this.confirmCircleMagnet;

this.magnetPosition.dragging = false;
currentFrame = this.videoHandle.currentFrameNumber;
hold(this.axisRight, 'on');
imshow(uint8(this.videoHandle.nextFrame(currentFrame)) ,'Parent',this.axisRight);

this.magnetPosition.circleHandle=line(nan,nan,'parent',this.axisRight,'color',[0.8941 0.1373 0.6157],'LineWidth', 7);

this.staticText.instructionsMagnet.Visible = 'on';
if this.magnetPosition.interruptValue
    return
end
end