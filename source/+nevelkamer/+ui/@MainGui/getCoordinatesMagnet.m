function coordinatesMagnet = getCoordinatesMagnet(this)
% Get screen coordinates

oldUnitsPointer = get(0,'Units');  % Get current units for hObject
set(0,'Units','pixels');    % Set units to unitType
coordinatesMagnet = get(0,'PointerLocation');    % Get propName property of hObject
set(0,'Units',oldUnitsPointer);


% get the position in pixel units
oldUnitsFigure = get(this.figureHandle,'Units');  % Get current units 
set(this.figureHandle,'Units','pixels');    % Set units to unitType
figurePosition = get(this.figureHandle,'Position');    % Get propName property
set(this.figureHandle,'Units',oldUnitsFigure); 

try
    oldUnitsAxes = get(this.axisRight.Children(1),'Units');  % Get current units 
set(this.axisRight.Children(1),'Units','pixels');    % Set units to unitType
axesPosition = get(this.axisRight.Children(1),'Position');    % Get propName property
set(this.axisRight.Children(1),'Units',oldUnitsAxes); 
axesLimits = [get(this.axisRight,'XLim').' get(this.axisRight.Children(1),'YLim').'];
catch
oldUnitsAxes = get(this.axisRight,'Units');  % Get current units 
set(this.axisRight,'Units','pixels');    % Set units to unitType
axesPosition = get(this.axisRight,'Position');    % Get propName property
set(this.axisRight,'Units',oldUnitsAxes); 
axesLimits = [get(this.axisRight,'XLim').' get(this.axisRight,'YLim').'];
end

% Compute offset and scaling for coords
offset = figurePosition(1:2)+axesPosition(1:2);
axesScale = diff(axesLimits)./axesPosition(3:4);

% Apply offsets and scaling
coordinatesMagnet = (coordinatesMagnet-offset).*axesScale+axesLimits(1,:);
end

