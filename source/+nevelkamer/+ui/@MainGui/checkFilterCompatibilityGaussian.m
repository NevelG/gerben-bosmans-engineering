function checkFilterCompatibiltyGaussian(this, src, eventdata)

if this.checkbox.contrastEnhancement.Value &&  this.checkbox.gaussian.Value
    warndlg('gaussian and contrast filters do not work well together so we unchecked the checkbox from the contrast for you');
    this.checkbox.contrastEnhancement.Value = 0;
end

end

