function pauseVideo(this, ~ ,~)

% Pauses the loop
this.togglePlay = false;

end
