function checkDisplayType(this, src ,eventdata)
displayType = this.dropdown.displayType.String(this.dropdown.displayType.Value,:);
if ~strcmpi(displayType,'Nicks Special Mix') && ~strcmpi(displayType, 'Show Magnet Position')
    if isempty(this.backgroundModel)
        opts = struct('WindowStyle','modal',...
            'Interpreter','tex');
        errordlg('\bf \fontsize{20} \color{magenta} No background model is made, therefore this filter is unavailable. You idiot.', ...
            'Idiot error', opts);
        this.dropdown.displayType.Value = 1;
    else
        if strcmpi(displayType, 'Show Background Model')
            this.updateRightImage;
        end
    end
elseif strcmpi(displayType, 'Show Magnet Position')
    this.updateRightImage;
end

end