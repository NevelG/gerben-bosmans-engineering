function confirmGhosting(this, src ,eventdata)
% save the frame at which the video is at currently before
% starting the processing
currentFrameNumberBeforeProcessing = this.videoHandle.currentFrameNumber;
% add some checks for the frames and return an warning if
% they are not round, then automatically round them
firstGhostingFrame = str2double(this.editText.firstGhostingFrame.String);
lastGhostingFrame = str2double(this.editText.lastGhostingFrame.String);

if isnan(firstGhostingFrame) || isnan(lastGhostingFrame)
    errordlg('Frame inputs have to be numbers');
    return
end
if firstGhostingFrame > lastGhostingFrame
    errordlg('The first frame cannot be higher than the last frame, interupting calculation of ghosted image');
    return
end

if firstGhostingFrame > this.videoHandle.numberOfFrames || lastGhostingFrame > this.videoHandle.numberOfFrames
    errordlg('The ghosting frame cannot be larger than the total number of frames');
    return
end

if firstGhostingFrame < 0 || lastGhostingFrame < 0
    errordlg('frame numbers cannot be negative');
    return
end

firstIsRound = mod(firstGhostingFrame,1);
lastIsRound = mod(lastGhostingFrame,1);
if firstIsRound && lastIsRound
    warndlg('The frame numbers are seposed to be round, automatically rounding the framenumber input');
    firstGhostingFrame = round(firstGhostingFrame);
    lastGhostingFrame = round(lastGhostingFrame);
end

if isempty(this.editText.firstGhostingFrame.String) || isempty(this.editText.lastGhostingFrame.String)
    warndlg('Please input a starting and end frame for ghosting, interupting the requested function until conditions are met');
    return
end

if isempty(this.backgroundModel)
    warndlg('Please determine a background model before attempting ghosting, interupting the requested function until conditions are met');
    return
end

this.ghostedImage = this.backgroundModel.ghostingDetection(firstGhostingFrame, lastGhostingFrame);

counter = 1;
for idx = firstGhostingFrame:lastGhostingFrame
    originalFrames(:,:,:,counter) = this.videoHandle.nextFrame(idx); %#ok<AGROW> performance is not an issue here
    counter = counter + 1;
end

this.originalFramesGhosting = [];
this.originalFramesGhosting = originalFrames;
% this is not used but functions purely to put the
% play back at the original position after the processing
this.videoHandle.nextFrame(currentFrameNumberBeforeProcessing);

% display the result in the right frame

ghostedImage = imclose(this.ghostedImage, strel('disk', 15));
ghostedImage = bwareaopen(ghostedImage, 80);
% this uses an icon to show in the image, but won't work if
% its incorrect filepath so left it out for now, if i have
% time i will autodetect some pathings of included images
%                 dialogIcon = imread('C:\Users\nicks\Documents\Project_TNE2\thumbs-up.jpg');
%                 msgbox('Unprocessed ghosting image is shown in the bottomright image, for further processing choose Mode -> Results from the menu','Success','custom',dialogIcon);
imshow(ghostedImage ,'Parent',this.axisRight);
msgbox('Standard processed ghosting image is shown in the bottomright image, for further processing and analysis choose Mode -> Results from the menu','Success');

end