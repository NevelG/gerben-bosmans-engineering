function updateRightImage(this, inputImage)

% this function updates the right image incase the tracking box is not
% selected. Track stuf is still in the code to handle the tracking when
% using the slider, as the slider cannot work with the fast track
% recognition.
displayType = this.dropdown.displayType.String{this.dropdown.displayType.Value};

switch displayType
    case 'Nicks Special Mix'
        % grab gaussian filter information
        sigmaValue = str2double(this.editText.sigmaValueBox.String);
        gaussianSize = str2double(this.editText.filterSizeBox.String);
        
        % grab the contrast information
        sizeStructuredElement = str2num(this.dropdown.filterSize.String{this.dropdown.filterSize.Value});   %#ok<ST2NM>
        
        % create the filteringObject
        filteringObject = nevelkamer.analysis.FilterImage(inputImage,...
            'gaussian', this.checkbox.gaussian.Value,...
            'gaussianSigma', sigmaValue,...
            'gaussianFilterSize', gaussianSize,...
            'contrast', this.checkbox.contrastEnhancement.Value,...
            'convertToGrayScale',this.checkbox.convertToGrayScale.Value,...
            'unsharpFilter',this.checkbox.unsharpFilter.Value,...
            'typeStructuredElement', this.dropdown.shapeStrel.String{this.dropdown.shapeStrel.Value},...
            'sizeStructuredElement', sizeStructuredElement,...
            'processingMode', this.dropdown.processingType.String{this.dropdown.processingType.Value});
        
        % grab the filtered image
        outputImage = filteringObject.outputImage;
        
    case 'Enhanced'
        trackMask = this.backgroundModel.showTracks(inputImage);
        outputImage = inputImage;
        outputImage(~trackMask) = 0;
        
    case 'Background Substraction'
        trackMask = this.backgroundModel.showTracks(inputImage);
        outputImage = inputImage;
        trackMask = repmat(trackMask, 1, 1 , 3);
        outputImage(~trackMask) = 0;
        
    case 'Show Background Model'
        outputImage = this.backgroundModel.backgroundModel;
        
    case 'Show Magnet Position'
        if ~isempty(this.imageMagnetPosition)
            if isempty(this.videoHandle.cropCoordinates)
                outputImage = this.imageMagnetPosition;
            else
                outputImage = this.imageMagnetPosition(...
                    this.videoHandle.cropCoordinates(2):this.videoHandle.cropCoordinates(4),...
                    this.videoHandle.cropCoordinates(1):this.videoHandle.cropCoordinates(3),:);
            end
            
            if isempty(this.externalFigure)
                displayAxis = this.axisRight;
            else
                try
                    displayAxis = this.externalFigure.CurrentAxes;
                catch
                    pause(0.5);
                    displayAxis = this.externalFigure.CurrentAxes;
                end
            end
            
            imshow(outputImage ,'Parent',displayAxis);
            return
        else
            this.togglePlay = false;
            errordlg('You need to draw the ROI around the magnet first before you can show it');
            % use the filtered object instead
            % grab gaussian filter information
            sigmaValue = str2double(this.editText.sigmaValueBox.String);
            gaussianSize = str2double(this.editText.filterSizeBox.String);
            
            % grab the contrast information
            sizeStructuredElement = str2num(this.dropdown.filterSize.String{this.dropdown.filterSize.Value});   %#ok<ST2NM>
            
            % create the filteringObject
            filteringObject = nevelkamer.analysis.FilterImage(inputImage,...
                'gaussian', this.checkbox.gaussian.Value,...
                'gaussianSigma', sigmaValue,...
                'gaussianFilterSize', gaussianSize,...
                'contrast', this.checkbox.contrastEnhancement.Value,...
                'convertToGrayScale',this.checkbox.convertToGrayScale.Value,...
                'unsharpFilter',this.checkbox.unsharpFilter.Value,...
                'typeStructuredElement', this.dropdown.shapeStrel.String{this.dropdown.shapeStrel.Value},...
                'sizeStructuredElement', sizeStructuredElement,...
                'processingMode', this.dropdown.processingType.String{this.dropdown.processingType.Value});
            
            % grab the filtered image
            outputImage = filteringObject.outputImage;
        end
end

if this.checkbox.trackRecognition.Value || strcmpi(displayType, 'Ghosted Tracks')
    numberOfFrames = round(str2double(this.editText.recognitionNumberOfFrames.String));
    if isnan(numberOfFrames) || numberOfFrames < 1
        warndlg('Number of frames and/or frame skipping is not a natural number, min value for frames = 1 and skipping = 0, rounding is done automatically');
        this.buttons.togglePlay = 0;
    end
    composedTracks = this.backgroundModel.liveGhosting(this.videoHandle.currentFrameNumber, numberOfFrames);
    
    % filter the composed tracks
    sizeStructuredElement = round(str2double(this.editText.sizeStructuredElement.String));
    pixelTresholdValue = round(str2double(this.editText.pixelThreshold.String));
    structuredElement = strel('disk',  sizeStructuredElement);
    
    % this is where we stop the gpuAcceleration and get
    % back to CPU because the regionprops from GPU is more
    % limited and we want to use perimeter and possibly
    % convexhull both not available in the CUDA code, also bwareaopen is
    % not available in the CUDA code
    if strcmpi(this.videoHandle.processingMode,'GPU Speed')
        composedTracks = gather(composedTracks); % pulls an array from GPU memory and writes it to an array in CPU memory
    end
    
    composedTracks = bwareaopen(composedTracks, pixelTresholdValue);
    composedTracks = imclose(composedTracks, structuredElement);

    if strcmpi(displayType, 'Ghosted Tracks')
        trackMask = composedTracks;
        outputImage = gather(inputImage);
        trackMask = repmat(trackMask, 1, 1 , 3);
        outputImage(~trackMask) = 0;
    end
end
% finally display the image based on the processing mode
% (as gpu array or uint8 cpu array for now added the
% brightness to both frames might be more usefull?
imadjustValue = this.slider.brightness.Value;

if isempty(this.externalFigure)
    displayAxis = this.axisRight;
else
    try
        displayAxis = this.externalFigure.CurrentAxes;
    catch
        pause(0.5);
        displayAxis = this.externalFigure.CurrentAxes;
    end
end

switch this.videoHandle.processingMode
    case 'GPU Speed'
        outputImage = imadjust(outputImage,[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',displayAxis);
    case 'CPU Accuracy'
        outputImage = imadjust(uint8(outputImage),[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',displayAxis);
    case 'CPU Speed'
        outputImage = imadjust(outputImage,[1-imadjustValue imadjustValue],[]);
        imshow(outputImage ,'Parent',displayAxis);
end

% draw bounding boxes incase the the detect track checkbox
% is checkedif this.checkbox.trackRecognition.Value
if this.checkbox.trackRecognition.Value
    % commented it out for now to save speed but might add
    % some of these properties later for further
    % discrimination of types
    detectedBlobs = regionprops(composedTracks, 'Centroid','BoundingBox','Perimeter','Area','MinorAxisLength','MajorAxisLength');
    
    if ~isempty(this.trainedModel)
        isAlpha = zeros(numel(detectedBlobs),1);
        for blobIdx = 1:numel(detectedBlobs)
            isAlpha(blobIdx,1) = this.trainedModel.predictFcn([detectedBlobs(blobIdx).MinorAxisLength,...
                detectedBlobs(blobIdx).MajorAxisLength,...
                detectedBlobs(blobIdx).Perimeter,...
                detectedBlobs(blobIdx).Area]);
            
            boundingBoxCoords = detectedBlobs(blobIdx).BoundingBox;
            if detectedBlobs(blobIdx).Area > (1.5*pixelTresholdValue)
                if isAlpha
                    % if alpha make bounding box blue
                    radiationTypeColor = [0 0 1];
                    radiationTypeText = 'Alpha';
                else % this means beta
                    % if beta make bounding box red
                    radiationTypeColor = [1 0 0];
                    radiationTypeText = 'Beta';
                end
            else
                radiationTypeColor = [1 1 0];
                radiationTypeText = 'To small to identify';
            end
                % draw the rectangular bounding box outputs are
                % not used but will cause the rectangle to
                % disappear when the next frame is displayed
                MyBoundingBox = rectangle(displayAxis, 'Position',boundingBoxCoords,...
                    'LineWidth', 1,...
                    'EdgeColor', radiationTypeColor);
                % display a radiationtype annotation above the
                % bounding box
                MyBoundingBox = text('Parent', displayAxis,...
                    'Position',[boundingBoxCoords(1) boundingBoxCoords(2)],...
                    'Color', radiationTypeColor,...
                    'VerticalAlignment', 'bottom',...
                    'FontSize', 8,...
                    'String', radiationTypeText);
        end
    else
        % get blob properties
        typeRatio = zeros(numel(detectedBlobs),1);
        for blobIdx = 1:numel(detectedBlobs)
            typeRatio(blobIdx,1) = (detectedBlobs(blobIdx).Area/detectedBlobs(blobIdx).Perimeter) * (detectedBlobs(blobIdx).MajorAxisLength  /  detectedBlobs(blobIdx).MinorAxisLength);
            
            boundingBoxCoords = detectedBlobs(blobIdx).BoundingBox;
            if this.hasStatisticsToolbox
                betaChance = this.calculateProbability((typeRatio(blobIdx,1)+this.betaTypeOffset),...
                    this.betaTypeMean, this.betaTypeDeviation, this.betaTypeInverseLambda);
                alphaChance = this.calculateProbability((typeRatio(blobIdx,1)+this.alphaTypeOffset),...
                    this.alphaTypeMean, this.alphaTypeDeviation, this.alphaTypeInverseLambda);
                isBeta = betaChance > alphaChance;
            else
                isBeta = typeRatio(blobIdx,1) < (this.betaTypeMean+2*this.betaTypeDeviation);
            end
            
            if isBeta
                % if beta make bounding box red
                radiationTypeColor = [1 0 0];
                radiationTypeText = 'Beta';
            else
                % if alpha make bounding box blue
                radiationTypeColor = [0 0 1];
                radiationTypeText = 'Alpha';
            end
            
            % draw the rectangular bounding box outputs are
            % not used but will cause the rectangle to
            % disappear when the next frame is displayed
            MyBoundingBox = rectangle(this.axisRight, 'Position',boundingBoxCoords,...
                'LineWidth', 1,...
                'EdgeColor', radiationTypeColor);
            % display a radiationtype annotation above the
            % bounding box
            MyBoundingBox = text('Parent', this.axisRight,...
                'Position',[boundingBoxCoords(1) boundingBoxCoords(2)],...
                'Color', radiationTypeColor,...
                'VerticalAlignment', 'bottom',...
                'FontSize', 8,...
                'String', radiationTypeText);
        end
    end
end

end
