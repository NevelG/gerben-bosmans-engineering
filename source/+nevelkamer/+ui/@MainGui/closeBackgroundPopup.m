function closeBackgroundPopup(this, src ,eventdata)

this.backgroundModelInfo.frameRange(1) = abs(str2double(this.popupHandle.values.editTextStartingFrame.String));
if ~isempty(this.popupHandle.values.editTextLastFrame.String)
    this.backgroundModelInfo.frameRange(2) = abs(str2double(this.popupHandle.values.editTextLastFrame.String));
    if this.backgroundModelInfo.frameRange(1) > this.backgroundModelInfo.frameRange(2)
        warndlg('Starting frame cannot be higher then last frame','Warning - Wrong input !!');
        return
    end
else
    this.backgroundModelInfo.frameRange(2) = abs(str2double(this.popupHandle.values.editTextStartingFrame.String));
end

if this.backgroundModelInfo.frameRange(1) > this.videoHandle.numberOfFrames || this.backgroundModelInfo.frameRange(2) > this.videoHandle.numberOfFrames
    warndlg('The frame number entered exceeds the total frame count of the video','Warning - Wrong input !!');
    return
end
% the currentframe number stuff makes sure the background model does not fastforward
% the video when u press play after the background calculation
currentFrameNumberBeforeProcessing = this.videoHandle.currentFrameNumber;
this.backgroundModel = nevelkamer.analysis.DetectTracks(this.videoHandle,...
    'frameRange', this.backgroundModelInfo.frameRange,...
    'backgroundModelType',this.backgroundModelInfo.type,...
    'processingMode', this.dropdown.processingType.String{this.dropdown.processingType.Value});
this.videoHandle.nextFrame(currentFrameNumberBeforeProcessing);
close(this.popupHandle.background);
this.popupHandle = [];

end