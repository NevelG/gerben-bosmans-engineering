        function initializeSplashScreen()
            screenInfo = groot;
            screenIdx = max(size(screenInfo.MonitorPositions,1));
            screenSize=[screenInfo.MonitorPositions(screenIdx,:)];
            splashScreen = figure;
            directoryImage=which('nevelkamer.ui.MainGui');
            imagePath = [directoryImage(1:end-18), 'splashScreen.png'];
            johan=imread(imagePath);
            imshow(johan);
            pause(0.1);
            set(splashScreen, 'ToolBar', 'none');
            set(splashScreen, 'MenuBar','none');
            set(splashScreen,'position',screenSize);
            try
                pause(0.1);
                splashScreen_h = get(handle(gcf),'JavaFrame');
                pause(0.3);
                set(splashScreen_h,'Maximized',2);
            catch
                set(splashScreen,'position',screenSize);
            end
            pause(1)
            NET.addAssembly('System.Speech');
            obj = System.Speech.Synthesis.SpeechSynthesizer;
            obj.Volume = 100;
            Speak(obj,'Welcome and prepare to be amazed. This program is brought to you by Gerben Bosmans Engineering.)');
            obj.Rate = 3.5;
            Speak(obj,'Disclaimer: Weholdnoliabilityforanydamagecausedbythisprogram, radiationhardwareorotherformsofdamage.)');
            gui=nevelkamer.ui.MainGui;
            close(1);
        end
        