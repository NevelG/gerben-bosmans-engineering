classdef VideoData < matlab.mixin.Copyable
    
    % This class is used to store the data from the video used in the GUI.
    % The main purpose of this class is to convert the time based MATLAB
    % VideoReader into a frame based video player for our purposes and to handle 
    % cameras with less stable frame rates. Saving all frames to be used later 
    % is sadly not possible due to memory issues, If the nextFrame function
    % is not fast enough it is possible to add a function to read out the
    % next "number of frame" that will be used in the processing or
    % videodisplaying but i think this will suffice.
    
    %***************** methods*************************
    % 1) the method obj.nextFrame allows the frame to be called using the
    %    frameNumber as input argument, the output will be the frame converted
    %    to doubles
    % 2) the method obj.updateVideoData is the function called when the
    %    constructor is used and simply sets up a videoreader object from
    %    matlab and loops through al of the frames to get the corresponding
    %    frame number to each time stamp. This frame number is the index of
    %    frameTimeLabel
    %

    properties
        filePath(1,:) nevelkamer.io.VideoFilePath = nevelkamer.io.VideoFilePath.empty % this is just a pointer and does not take up any extra memory
        video % this is the video object from matlab's VideoReader
        cropCoordinates(1,:) double = []
        processingMode(1,:) char = ''
    end
    
    properties (SetAccess = protected)
        name(1,:) char = '' % this is not a pointer but memory management wise this is not important and this could be used to display as static tekst
        
        frameTimeLabel(1,:) double = []
        numberOfFrames(1,1) double = 0       
        frameRate(1,1) double = 0
        videoLength(1,1) double = 0
        resolution(1,:) double = []
        bitsPerPixel(1,1) double = 0
        format(1,:) char = ''
        currentFrameNumber(1,1) double = 0
    end
    
    properties (Constant)
       VIDEOLENGTH_UNIT = 'seconds' 
       FRAMERATE_UNIT = 'frames per second'
       RESOLUTION_FORMAT = '[Width Height]'
    end
     
    
    methods
        % constructor
        function newVideo = VideoData(inputFilePath)
            assert(isa(inputFilePath, 'nevelkamer.io.VideoFilePath'), 'Input file is of the wrong format, required input = nevelkamer.io.VideoFilePath');
            if nargin>0 && ~isempty(inputFilePath)
                newVideo.filePath = inputFilePath;
                newVideo.updateVideoData;
            else
                warning('FilePath was empty or no input arguments were given, the created object will be empty');
            end
        end
        % might need to convert this function to a static method if calling
        % it from outside of the gui gives errors
        function outputFrame = nextFrame(this,frameNumber)
            currentTimeFrame = this.frameTimeLabel(1, frameNumber);
            this.video.CurrentTime = currentTimeFrame;
            outputFrame = readFrame(this.video);
            if ~isempty(this.cropCoordinates)
                outputFrame = outputFrame(this.cropCoordinates(2):this.cropCoordinates(4),this.cropCoordinates(1):this.cropCoordinates(3),:);
            end
            
            switch this.processingMode
                case 'GPU Speed'
                    outputFrame = gpuArray(outputFrame);
                case 'CPU Speed'
                    outputFrame = uint8(outputFrame);
                case 'CPU Accuracy'
                    outputFrame = double(outputFrame);
            end
        end
        
        function currentFrameNumber = get.currentFrameNumber(this)
                currentFrameNumber = find(this.frameTimeLabel == this.video.CurrentTime, 1, 'First');
                if isempty(currentFrameNumber)
                   differenceVector = abs(this.frameTimeLabel - this.video.CurrentTime);
                   [~, currentFrameNumber] = min(differenceVector);
                end
        end
    end % end methods (no atrributes)
  
    
    methods (Access = protected)
        function updateVideoData(this)
            % initiate the videoreader object and reallocate the data for
            % readability purposes
            initialVideoObject = VideoReader(this.filePath.filePath);
            this.video = initialVideoObject;
            this.name = this.filePath.name;
            this.frameRate = initialVideoObject.FrameRate;
            this.resolution = [initialVideoObject.Width initialVideoObject.Height];
            this.bitsPerPixel = initialVideoObject.BitsPerPixel;
            this.format = initialVideoObject.VideoFormat;
            this.videoLength = initialVideoObject.Duration;
            
            % preallocate a very large vector to avoid dynamic memory
            % allocation slowing everything down
            frameTimingVector = zeros(1,1000000);           
            frameIdx = 1;
            
            % setup a waitbar for the loading as this part can take some
            % time for longer videos
            progressBar = waitbar(0,'0', 'Name', 'Loading your video and preparing for analysis ...');
            screenInfo = groot;
            numberOfMonitors = size(screenInfo.MonitorPositions, 1);
            if numberOfMonitors > 1
                % set the progressBar on second screen
%                 % does not work yet
%                 progressBar.Position = [(screenInfo.MonitorPositions(numberOfMonitors, 1)+screenInfo.MonitorPositions(numberOfMonitors, 3)*0.3 )...
%                     (screenInfo.MonitorPositions(numberOfMonitors, 2)+screenInfo.MonitorPositions(numberOfMonitors, 4)*0.46) ...
%                     (screenInfo.MonitorPositions(numberOfMonitors, 3)*0.4) ...
%                     (screenInfo.MonitorPositions(numberOfMonitors, 4)*0.08)];
            end
            
            
            while hasFrame(initialVideoObject)
                frameTimingVector(1, frameIdx) = initialVideoObject.CurrentTime;               
                currentFrame = readFrame(initialVideoObject);               
                frameIdx = frameIdx + 1;
                percentageProgression =  round((initialVideoObject.CurrentTime / initialVideoObject.Duration) * 100);
                waitbar(initialVideoObject.CurrentTime / initialVideoObject.Duration, progressBar, sprintf('%d', percentageProgression));               
            end
            delete(progressBar);
            secondZero = find(frameTimingVector == 0, 2, 'first');
            this.frameTimeLabel = frameTimingVector(1,1:secondZero(2)-1);
            
            this.numberOfFrames = frameIdx-1;
        end
    end % end protected methods
end % end class
    
       
