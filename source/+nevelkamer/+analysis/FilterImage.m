classdef FilterImage < matlab.mixin.Copyable
    % Very simple class that takes a filepath as input and does some small
    % checks if the file format is correct for our application (videofile)
    % and if we have read permission to access it
    
    properties
        inputImage = []
        processingMode(1,:) char = ''
        gaussianFilter(1,1) logical = false % get the value for this from checkboxes
        contrastFilter(1,1) logical = false
        convertToGrayScale(1,1) logical = false
        unsharpFilter(1,1) logical = false 
        gaussianFilterSize(1,1) double = 5 % get this value from a dropbox or edit text blocks
        gaussianSigma(1,1) double = 3
        typeStructuredElement(1,:) char = 'disk'
        sizeStructuredElement(1,:) double = []
    end
    
    properties (SetAccess = protected)
        outputImage = []
    end
    
    methods
        function newObj = FilterImage(varargin)
            newObj.inputImage = varargin{1};
            for idx = 2:2:(numel(varargin)-1)
                if ischar(varargin{idx})
                    if     strcmpi(varargin{idx}, 'gaussian')
                        newObj.gaussianFilter = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'contrast')
                        newObj.contrastFilter = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'gaussianSigma')
                        newObj.gaussianSigma = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'gaussianFilterSize')
                        newObj.gaussianFilterSize = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'typeStructuredElement')
                        newObj.typeStructuredElement = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'sizeStructuredElement')
                        newObj.sizeStructuredElement = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'convertToGrayScale')
                        newObj.convertToGrayScale = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'unsharpFilter')
                        newObj.unsharpFilter = varargin{idx+1};
                    elseif strcmpi(varargin{idx}, 'processingMode')
                        newObj.processingMode = varargin{idx+1};
                    end
                end
            end
            newObj.updateFilters;
        end
        
        function updateFilters(this)
            processedImage = this.inputImage;
            
            if this.convertToGrayScale
                if numel(size(processedImage)) > 2
                    switch this.processingMode
                        case 'CPU Accuracy'
                            % matlabs rbg2gray does not function on doubles
                            % and the uint8 conversion -> rgb2gray ->
                            % double increases the computation time so here
                            % we do the conversion ourselfs which is
                            % slightly more inefficient than rgb2gray
                            % but saves the time of the conversions
                            processedImage =...
                                0.2989 .* processedImage(:,:,1) +...
                                0.5870 .* processedImage(:,:,2) + ...
                                0.1140 .* processedImage(:,:,3);
                        otherwise
                            processedImage = rgb2gray(processedImage);
                    end
                else
                    % this is just a commandline comment, will change this to a popup where
                    % you can click "ok" to disable checkbox
                    warning('Image input is already a grayscale image, please disable the converToGrayScale checkbox');
                end
            end
            
            if this.gaussianFilter
                processedImage = imgaussfilt(processedImage,this.gaussianSigma,'FilterSize',this.gaussianFilterSize);
            end
            
            if this.contrastFilter
                % This uses the difference between the top- and bottonhat
                % image (basic image morphology) with the original to enhance the
                % contrast. This facilitates the detection of tracks and might improve later processing.
                if ~(strcmpi(this.typeStructuredElement, 'line'))
                    structuredElement = strel(this.typeStructuredElement,this.sizeStructuredElement);
                else
                    structuredElement = strel(this.typeStructuredElement,this.sizeStructuredElement(1), this.sizeStructuredElement(2));
                end
                tophatImage = imtophat(processedImage,structuredElement);
                bothatImage = imbothat(processedImage,structuredElement);
                
                % gpuArrays cant handle imsubtract and imadd but it is used
                % for the "CPU Speed" to handle underflow from integer
                % subtraction and overflow from integer addition, CPU
                % accuracy uses double and does not get affected by the
                % underflow and overflow so normal minus has less overhead
                % and speeds up the calcultion
                if strcmpi(this.processingMode, 'CPU Speed')
                    processedImage = imsubtract(imadd(processedImage,tophatImage), bothatImage);
                else
                    processedImage = (processedImage+tophatImage) - bothatImage;
                end
                
            end
            
            if this.unsharpFilter
                % if the gaussian filter was already calculated we reuse it
                % here to save computation time
                if this.gaussianFilter
                    unsharpFilterImage = imabsdiff(processedImage, processedImage);
                else
                    unsharpFilterImage = imgaussfilt(processedImage,this.gaussianSigma,'FilterSize',this.gaussianFilterSize);
                    unsharpFilterImage = imabsdiff(processedImage, unsharpFilterImage);
                end
                % for explaination see above with contrast filter (same
                % reason)
                if strcmpi(this.processingMode, 'CPU Speed')
                    processedImage = imsubtract(processedImage, unsharpFilterImage);
                else
                    processedImage = processedImage - unsharpFilterImage;
                end
            end
            
            if this.contrastFilter && this.gaussianFilter
                warning('using both contrast and gaussian filters at the same time just negates the effect of both filters resulting in an increased computational time without any gain');
            end
            
            this.outputImage = processedImage;
        end
    end % methods (no attributes)
end % end of class