classdef DetectTracks < matlab.mixin.Copyable
    % This class will process the images to produce subtracted images
    % containing mostly tracks with minimal amount of noise and convert the
    % detected tracks into vector of coordinates, this class however does
    % not take in account ghosting and works frame by frame, the
    % combination of several ghosted tracks is done by the IdentifyTracks
    % class which will be made after this class functions correctly

    properties
        videoObjectHandle
        backgroundModel(:,:,:) = []
        treshold(1,1) double = 0.30
        frameRange(1,:) double = []
        backgroundModelType(1,:) char = ''
        liveGhostingInitiated(1,1) logical = false
    end

    properties (SetAccess = protected)
        liveGhostingMatrix(:,:,:) = []
        ghostedFrames(:,:,:) = []
        normalizationValue(1,1) = 0
        processingMode(1,:) char = ''
    end

    methods
        function newObj= DetectTracks(varargin)
            newObj.videoObjectHandle = varargin{1};
            %******* just put it ready for expanding the input of the varargin *****
            for idx = 2:2:(numel(varargin)-1)
                if ischar(varargin{idx})
                    if     strcmpi(varargin{idx}, 'BackgroundModel')
                        newObj.backgroundModel = varargin{idx+1};
                    elseif  strcmpi(varargin{idx}, 'frameRange')
                        newObj.frameRange = varargin{idx+1};
                    elseif  strcmpi(varargin{idx}, 'treshold')
                        newObj.treshold = varargin{idx+1};
                    elseif  strcmpi(varargin{idx}, 'backgroundModelType')
                        newObj.backgroundModelType = varargin{idx+1};
                    elseif  strcmpi(varargin{idx}, 'processingMode')
                        newObj.processingMode = varargin{idx+1};
                    end
                end
            end
            newObj.calculateModelBackground;
        end

        function trackMask = showTracks(this, inputImage)
            if isempty(this.videoObjectHandle.cropCoordinates)
                foregroundImage = sum( abs( inputImage - this.backgroundModel ), 3 );
            else
                foregroundImage = sum( abs( inputImage - this.backgroundModel(...
                    this.videoObjectHandle.cropCoordinates(2):...
                    this.videoObjectHandle.cropCoordinates(4),...
                    this.videoObjectHandle.cropCoordinates(1):...
                    this.videoObjectHandle.cropCoordinates(3),:)), 3 );
            end
            % normalize the foregroundImage
            foregroundImage = foregroundImage / this.normalizationValue;

            % subtract background from foreground
            trackMask = foregroundImage > this.treshold;

            if strcmpi(this.backgroundModelType, 'Gaussian Mixture Model')
                % apply the gaussian model and keep it evolving.
                trackMask = this.backgroundModel.apply(im, 'LearningRate',0);
                % Process mask
                if true
                    trackMask = cv.dilate(cv.erode(trackMask));
                end
            end
        end

        function outputImage = liveGhosting(this, frameNumber, frameCount)
            % determine the offsets to set the starting frame etc.
            oddNumberOfFrames = bitget(frameCount,1);
            if oddNumberOfFrames
                startingOffset = ceil(frameCount/2) - 1;
                endingOffset = startingOffset;
            else
                % because we need the frame itself aswel to get to our
                % number of frames
                startingOffset = frameCount/2;
                endingOffset = (frameCount/2) - 1;
            end

            if frameNumber > (frameCount/2 + 1)
                coordinateVectorTracks = [];
                for idx = (frameNumber-startingOffset):(frameNumber+endingOffset)
                    inputImage = this.videoObjectHandle.nextFrame(idx);

                    if isempty(this.videoObjectHandle.cropCoordinates)
                        foregroundImage = sum( abs( inputImage - this.backgroundModel ), 3 );
                    else
                        foregroundImage = sum( abs( inputImage - this.backgroundModel(...
                            this.videoObjectHandle.cropCoordinates(2):...
                            this.videoObjectHandle.cropCoordinates(4),...
                            this.videoObjectHandle.cropCoordinates(1):...
                            this.videoObjectHandle.cropCoordinates(3),:)), 3 );
                    end
                    % normalize the foregroundImage
                    foregroundImage = foregroundImage / this.normalizationValue;
                    % subtract background from foreground
                    trackMask = foregroundImage > this.treshold;
                    trackMask = medfilt2(trackMask, [3 3]);

                    % get the coordinates of the track
                    [rows, columns] = find(trackMask == 1);
                    if isempty(coordinateVectorTracks)
                        coordinateVectorTracks = [ columns rows];
                    else
                        coordinateVectorTracks = [ coordinateVectorTracks; columns rows];
                    end
                end

                imageSize = size(inputImage);

                switch this.processingMode
                    case 'GPU Speed'
                        outputImage = zeros(imageSize(1), imageSize(2),'logical','gpuArray');
                    otherwise
                        outputImage = zeros(imageSize(1), imageSize(2),'logical');
                end
                % remove all dublicate coordinates to speed up the sub2ind
                % and especially the for loop incase the sub2ind fails.
                uniqueCoordinates = unique(coordinateVectorTracks, 'rows');

                try
                    outputImage(sub2ind(imageSize,uniqueCoordinates(:,2),uniqueCoordinates(:,1))) = 1;
                catch
                    % this is a lot slower but sub2ind occassionaly gives an
                    % error if the data set gets to large
                    for idx = 1:size(uniqueCoordinates,1)
                        outputImage(uniqueCoordinates(idx,2),uniqueCoordinates(idx,1)) = 1;
                    end
                end
            end
        end

        function ghostedImage = ghostingDetection(this, firstFrame, lastFrame)
            % for this we will always work with double precision instead of
            % the speeded up versions because this image is used in the
            % results for further analysis and speed is no longer an issue
            coordinateVectorTracks = [];
            progressBar = waitbar(0,'0', 'Name', 'Determining normalization value');
            totalFrames = numel(firstFrame:lastFrame);
            % determine normalization factor
            maxNormalizationValue = zeros(totalFrames,1);

            for idx = firstFrame:lastFrame
                progression = idx-firstFrame+1;
                progressionString = sprintf('%d out of %d', progression, totalFrames);
                waitbar(progression / totalFrames, progressBar, progressionString);

                inputImage = this.videoObjectHandle.nextFrame(idx);

                switch this.processingMode
                    case 'GPU Speed'
                        inputImage = double(gather(inputImage));
                        backgroundForSubtraction = double(gather(this.backgroundModel));
                    case 'CPU Speed'
                        inputImage = double(inputImage);
                        backgroundForSubtraction = double(this.backgroundModel);
                    case 'CPU Accuracy'
                        backgroundForSubtraction = this.backgroundModel;
                end

                if isempty(this.videoObjectHandle.cropCoordinates)
                    foregroundImage = sum( abs( inputImage - backgroundForSubtraction ), 3 );
                else
                    foregroundImage = sum( abs( inputImage - backgroundForSubtraction(...
                        this.videoObjectHandle.cropCoordinates(2):...
                        this.videoObjectHandle.cropCoordinates(4),...
                        this.videoObjectHandle.cropCoordinates(1):...
                        this.videoObjectHandle.cropCoordinates(3),:)), 3 );
                end
            end
            delete(progressBar);
            progressBar = waitbar(0,'0', 'Name', 'Composing tracks from frames');
            for idx = firstFrame:lastFrame
                progression = idx-firstFrame+1;
                progressionString = sprintf('%d out of %d', progression, totalFrames);
                waitbar(progression / totalFrames, progressBar, progressionString);
                inputImage = this.videoObjectHandle.nextFrame(idx);
                switch this.processingMode
                    case 'GPU Speed'
                        inputImage = double(gather(inputImage));
                        backgroundForSubtraction = double(gather(this.backgroundModel));
                    case 'CPU Speed'
                        inputImage = double(inputImage);
                        backgroundForSubtraction = double(this.backgroundModel);
                    case 'CPU Accuracy'
                        backgroundForSubtraction = this.backgroundModel;
                end

                if isempty(this.videoObjectHandle.cropCoordinates)
                    foregroundImage = sum( abs( inputImage - backgroundForSubtraction ), 3 );
                else
                    foregroundImage = sum( abs( inputImage - backgroundForSubtraction(...
                        this.videoObjectHandle.cropCoordinates(2):...
                        this.videoObjectHandle.cropCoordinates(4),...
                        this.videoObjectHandle.cropCoordinates(1):...
                        this.videoObjectHandle.cropCoordinates(3),:)), 3 );
                end
                % normalize the foregroundImage
                foregroundImage = foregroundImage / this.normalizationValue;
                % subtract background from foreground
                trackMask = foregroundImage > this.treshold;
                if strcmpi(this.processingMode, 'GPU Speed')
                    trackMask = gather(trackMask);
                end

                trackMask = bwareaopen(trackMask,4);
                numberOfBlobs = regionprops(trackMask);
                if numel(numberOfBlobs) > 2
                    subtractedImage = rgb2gray(inputImage) .* trackMask;
                    probabilityOfTrackImage = fibermetric(subtractedImage, 10 ,'ObjectPolarity','bright');
                    trackImage = probabilityOfTrackImage > 0.1;
                else
                    trackImage = trackMask;
                end

                % get the coordinates of the track
                [rows, columns] = find(trackImage == 1);
                if isempty(coordinateVectorTracks)
                    coordinateVectorTracks = [ columns rows];
                else
                    coordinateVectorTracks = [ coordinateVectorTracks; columns rows];
                end
            end
            delete(progressBar);
            imageSize = size(inputImage);
            ghostedImage = zeros(imageSize(1), imageSize(2));

            uniqueCoordinates = unique(coordinateVectorTracks, 'rows');
            try
                ghostedImage(sub2ind(size(ghostedImage),uniqueCoordinates(:,2),uniqueCoordinates(:,1))) = 1;
            catch
                % this is a lot slower but sub2ind occassionaly gives an
                % error if the data set gets to large
                for idx = 1:size(coordinateVectorTracks,1)
                    x = uniqueCoordinates(idx,1);
                    y = uniqueCoordinates(idx,2);
                    ghostedImage(y,x) = 1;
                end
            end
        end

        function [trainedClassifier, validationAccuracy] = trainRecognitionModel(this, inputMatrix, modelType, subType)
            inputTable = array2table(inputMatrix,...
                'VariableNames', {'MinorAxis', 'MajorAxis',...
                'Perimeter', 'Area', 'RadiationParameter'}); % radiation parameter 1 for alpha 0 for beta

            predictorNames = {'MinorAxis', 'MajorAxis', 'Perimeter', 'Area'};
            predictors = inputTable(:, predictorNames);
            response = inputTable.RadiationParameter;
            isCategoricalPredictor = [false, false, false, false];

            if strcmpi(modelType, 'Support Vector Machine')
                kernelType = subType;
                classificationSVM = fitcsvm(...
                    predictors, ...
                    response, ...
                    'KernelFunction', kernelType, ...
                    'PolynomialOrder', [], ...
                    'KernelScale', 'auto', ...
                    'BoxConstraint', 1, ...
                    'Standardize', true, ...
                    'ClassNames', [0; 1]);

                predictorExtractionFcn = @(x) array2table(x, 'VariableNames', predictorNames);
                svmPredictFcn = @(x) predict(classificationSVM, x);
                trainedClassifier.predictFcn = @(x) svmPredictFcn(predictorExtractionFcn(x));

                % Add additional fields to the result struct
                trainedClassifier.ClassificationSVM = classificationSVM;


                % Extract predictors and response
                % This code processes the data into the right shape for training the
                % model.
                % Convert input to table
                inputTable = array2table(inputMatrix,...
                    'VariableNames', {'MinorAxis', 'MajorAxis',...
                    'Perimeter', 'Area', 'RadiationParameter'}); % radiation parameter 1 for alpha 0 for beta

                predictorNames = {'MinorAxis', 'MajorAxis', 'Perimeter', 'Area'};
                predictors = inputTable(:, predictorNames);
                response = inputTable.RadiationParameter;
                isCategoricalPredictor = [false, false, false, false];

                % Perform cross-validation
                partitionedModel = crossval(trainedClassifier.ClassificationSVM, 'KFold', 5);

                % Compute validation predictions
                [validationPredictions, validationScores] = kfoldPredict(partitionedModel);

                % Compute validation accuracy
                validationAccuracy = 1 - kfoldLoss(partitionedModel, 'LossFun', 'ClassifError');
            elseif strcmpi(modelType, 'K Nearest Neighbors')
                if isa(subType, 'char')
                    numberOfNeighbors = str2double(subType);
                else
                    numberOfNeighbors = subType;
                end
                classificationKNN = fitcknn(...
                    predictors, ...
                    response, ...
                    'Distance', 'Euclidean', ...
                    'Exponent', [], ...
                    'NumNeighbors', numberOfNeighbors, ...
                    'DistanceWeight', 'Equal', ...
                    'Standardize', true, ...
                    'ClassNames', [0; 1]);

                predictorExtractionFcn = @(x) array2table(x, 'VariableNames', predictorNames);
                knnPredictFcn = @(x) predict(classificationKNN, x);
                trainedClassifier.predictFcn = @(x) knnPredictFcn(predictorExtractionFcn(x));

                % Add additional fields to the result struct
                trainedClassifier.ClassificationKNN = classificationKNN;

                % Extract predictors and response
                % This code processes the data into the right shape for training the
                % model.
                % Convert input to table
                inputTable = array2table(inputMatrix, ...
                    'VariableNames', {'MinorAxis', 'MajorAxis',...
                    'Perimeter', 'Area', 'RadiationParameter'});

                predictorNames = {'MinorAxis', 'MajorAxis', 'Perimeter', 'Area'};
                predictors = inputTable(:, predictorNames);
                response = inputTable.RadiationParameter;
                isCategoricalPredictor = [false, false, false, false];

                % Perform cross-validation
                partitionedModel = crossval(trainedClassifier.ClassificationKNN, 'KFold', 5);

                % Compute validation predictions
                [validationPredictions, validationScores] = kfoldPredict(partitionedModel);

                % Compute validation accuracy
                validationAccuracy = 1 - kfoldLoss(partitionedModel, 'LossFun', 'ClassifError');
            end
        end
    end % methods (no attributes)

    methods (Access = protected)
        function calculateModelBackground(this)
            % loop through the image
            % preallocate the memory needed for saving the frames
            if strcmpi(this.backgroundModelType, 'Averaged Frames')
                sizeFrame = size(this.videoObjectHandle.nextFrame(1));
                % this is the fasted and most efficient way of doing the
                % background calculation, however if your computer does not
                % have enough memory it'll start messing up so we use a try and
                % catch method here especially when using the GPU method
                % running out of memory is a more frequent problem
                try
                    switch this.processingMode
                        case 'GPU Speed'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)),'uint8', 'gpuArray');
                        case 'CPU Speed'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)), 'uint8');
                        case 'CPU Accuracy'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)), 'double');
                    end

                    progressBar = waitbar(0,'0', 'Name', 'Number of frames processed');
                    totalFrames = numel(this.frameRange(1):this.frameRange(2));
                    for innerLoop = this.frameRange(1):this.frameRange(2)
                        progression = innerLoop-this.frameRange(1)+1;
                        backgroundFrames(:,:,:,progression) = uint8(this.videoObjectHandle.nextFrame(innerLoop));
                        progressionString = sprintf('%d out of %d', progression, totalFrames);
                        waitbar(progression / totalFrames, progressBar, progressionString);
                    end
                    delete(progressBar);
                    progressBar = waitbar(0,'0', 'Name', ' Calculating normalization values - this allows cropping');

                    switch this.processingMode
                        case 'GPU Speed'
                            this.backgroundModel = uint8(mean( backgroundFrames, 4 ));
                            %        this.backgroundModel = uint8( averagedBackgroundModel);
                        case 'CPU Speed'
                            this.backgroundModel = uint8(mean(backgroundFrames, 4 ));
                            %       this.backgroundModel = uint8(averagedBackgroundModel);
                        case 'CPU Accuracy'
                            this.backgroundModel = mean( backgroundFrames, 4 );
                    end

                    % incase of a stack overflow error we will split up the
                    % averaging over several loops
                catch
                    backgroundFrames = [];

                    warndlg('Stackoverflow error due to to many background frames, splitting up for loops, this might take some time');
                    try
                        delete(progressBar);
                    catch
                        warning('stackoverflow - deleting progressBar not possible');
                    end
                    progressBar = waitbar(0,'0', 'Name', ' Stackoverflow - splitting for loops this time');
                    totalFrames = numel(this.frameRange(1):this.frameRange(2));
                    splittingLoops = mod(totalFrames,20);
                    lastFrame = this.frameRange(2) - splittingLoops;
                    newTotalFrames = numel(this.frameRange(1):lastFrame);
                    outerLoopCounter = 1;
                    frameNumber = this.frameRange(1);
                    progressCounter = 1;

                    switch this.processingMode
                        case 'GPU Speed'
                            averagedBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame),'uint8', 'gpuArray');
                        case 'CPU Speed'
                            averagedBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame), 'uint8');
                        case 'CPU Accuracy'
                            averagedBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame), 'double');
                    end

                    for outerLoop = this.frameRange(1):20:lastFrame
                        switch this.processingMode
                            case 'GPU Speed'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'uint8', 'gpuArray');
                            case 'CPU Speed'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'uint8');
                            case 'CPU Accuracy'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'double');
                        end

                        for innerLoop = 1:20
                            backgroundFrames(:,:,:,innerLoop) = this.videoObjectHandle.nextFrame(frameNumber);
                            progressionString = sprintf('%d out of %d', progressCounter, newTotalFrames);
                            waitbar(progressCounter / newTotalFrames, progressBar, progressionString);
                            frameNumber = frameNumber + 1;
                            progressCounter = progressCounter + 1;
                        end
                        averagedBackground(:,:,:,outerLoopCounter) = mean( backgroundFrames, 4 );
                        outerLoopCounter = outerLoopCounter + 1;
                    end
                    delete(progressBar);

                    progressBar = waitbar(0,'0', 'Name', ' Calculating normalization values - this allows cropping');

                    switch this.processingMode
                        case 'GPU Speed'
                            this.backgroundModel =  uint8(mean( averagedBackground, 4 ));
                        case 'CPU Speed'
                            this.backgroundModel =  uint8(mean(averagedBackground, 4 ));
                        case 'CPU Accuracy'
                            this.backgroundModel =  mean( averagedBackground, 4 );
                    end

                end
            elseif strcmpi(this.backgroundModelType, 'Single Frame')
                this.backgroundModel = this.videoObjectHandle.nextFrame(this.frameRange(1));
                progressBar = waitbar(0,'0', 'Name', ' Calculating normalization values - this allows cropping');

            elseif strcmpi(this.backgroundModelType, 'Median Background Model')
                sizeFrame = size(this.videoObjectHandle.nextFrame(1));
                % this is the fasted and most efficient way of doing the
                % background calculation, however if your computer does not
                % have enough memory it'll start messing up so we use a try and
                % catch method here, especially when using the GPU method
                % running out of memory is a more frequent problem
                runErrorMode = false;

                try
                    switch this.processingMode
                        case 'GPU Speed'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)),'uint8', 'gpuArray');
                        case 'CPU Speed'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)), 'uint8');
                        case 'CPU Accuracy'
                            backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):this.frameRange(2)), 'double');
                    end

                    progressBar = waitbar(0,'0', 'Name', 'Number of frames processed');
                    totalFrames = numel(this.frameRange(1):this.frameRange(2));
                    for innerLoop = this.frameRange(1):this.frameRange(2)
                        progression = innerLoop-this.frameRange(1)+1;
                        backgroundFrames(:,:,:,progression) = this.videoObjectHandle.nextFrame(innerLoop);
                        progressionString = sprintf('%d out of %d', progression, totalFrames);
                        waitbar(progression / totalFrames, progressBar, progressionString);
                    end
                    delete(progressBar);

                    progressBar = waitbar(0,'0', 'Name', ' Calculating normalization values - this allows cropping');

                    try
                        this.backgroundModel = median( backgroundFrames, 4 );
                    catch
                        runErrorMode = true;
                    end


                    % incase of a stack overflow error we will split up the
                    % averaging over several loops
                catch
                    runErrorMode = true;
                end
                % this is incase the stackoverflow error only happens
                % at the determination of the average or median.
                if runErrorMode
                    convertedToCPU = false;
                    backgroundFrames = [];

                    warndlg('Stackoverflow error due to to many background frames, splitting up for loops, this might take some time');
                    try
                        delete(progressBar);
                    catch
                        warning('tried to delete progress bar to split up loops but there was no progress bar, just starting the splitting');
                    end
                    progressBar = waitbar(0,'0', 'Name', ' Stackoverflow - splitting for loops this time');
                    totalFrames = numel(this.frameRange(1):this.frameRange(2));
                    splittingLoops = mod(totalFrames,20);
                    lastFrame = this.frameRange(2) - splittingLoops;
                    newTotalFrames = numel(this.frameRange(1):lastFrame);
                    outerLoopCounter = 1;
                    frameNumber = this.frameRange(1);
                    progressCounter = 1;

                    switch this.processingMode
                        case 'GPU Speed'
                            medianBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame), 'uint8', 'gpuArray');
                        case 'CPU Speed'
                            medianBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame), 'uint8');
                        case 'CPU Accuracy'
                            medianBackground = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), numel(this.frameRange(1):20:lastFrame), 'double');
                    end

                    for outerLoop = this.frameRange(1):20:lastFrame
                        switch this.processingMode
                            case 'GPU Speed'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'uint8', 'gpuArray');
                            case 'CPU Speed'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'uint8');
                            case 'CPU Accuracy'
                                backgroundFrames = zeros(sizeFrame(1),sizeFrame(2),sizeFrame(3), 20, 'double');
                        end

                        for innerLoop = 20:-1:1 % reverse loop to increase speed (even faster than preallocation as we don't need to grab an extra frame in advance
                            backgroundFrames(:,:,:,innerLoop) = this.videoObjectHandle.nextFrame(frameNumber);
                            progressionString = sprintf('%d out of %d', progressCounter, newTotalFrames);
                            waitbar(progressCounter / newTotalFrames, progressBar, progressionString);
                            frameNumber = frameNumber + 1;
                            progressCounter = progressCounter + 1;
                        end

                        try
                        medianBackground(:,:,:,outerLoopCounter) = median( backgroundFrames, 4 );
                        catch
                            % preallocate the for loop matrix
                            medianBackground = zeros(size(backgroundFrames,1),...
                                size(backgroundFrames,2),size(backgroundFrames,3));
                            % get the matrix back from the GPU as the GPU
                            % clearly does not have the memory to handle
                            % the matrices at this point
                            switch this.processingMode
                                case 'GPU Speed'
                                    backgroundFramesCPU = gather(backgroundFrames);
                                    % have a logical value to state its currently a
                                    % CPU array
                                    convertedToCPU = true;
                                    try
                                        medianBackground(:,:,:,outerLoopCounter) = median( backgroundFramesCPU, 4 );
                                        conversionWasEnough = true;
                                    catch
                                        conversionWasEnough = false;
                                    end
                                otherwise
                                    convertedToCPU = false;
                                    conversionWasEnough = false;

                            end

                            if ~conversionWasEnough
                            for rowIdx = 1:size(backgroundFramesCPU,1)
                                for columnIdx = 1:size(backgroundFramesCPU,2)
                                    medianBackground(rowIdx,columnIdx,:, outerLoopCounter) = ...
                                         median( backgroundFramesCPU(rowIdx,columnIdx,:, 4 ));
                                end
                            end
                            end
                        end
                        outerLoopCounter = outerLoopCounter + 1;
                    end
                    delete(progressBar);
                    progressBar = waitbar(0,'0', 'Name', ' Calculating normalization values - this allows cropping');

                    try
                        if ~convertedToCPU
                            this.backgroundModel = median( medianBackground, 4 );
                        elseif convertedToCPU
                            this.backgroundModel = uint8(gpuArray( median( medianBackground, 4 )) );
                        else
                            warning('no flag found for CPU or GPU conversion, something must have gone wrong');
                        end
                    catch
                        % preallocate the for loop matrix
                        intermedBackgroundModel = zeros(size( medianBackground,1),...
                            size( medianBackground,2),size( medianBackground,3));
                        for rowIdx = 1:size( medianBackground,1)
                            for columnIdx = 1:size( medianBackground,2)
                                intermedBackgroundModel(rowIdx,columnIdx,:) = ...
                                    median(  medianBackground(rowIdx,columnIdx,:, 4 ));
                            end
                        end

                        if ~convertedToCPU
                            this.backgroundModel = intermedBackgroundModel;
                        elseif convertedToCPU
                            this.backgroundModel = uint8(gpuArray(intermedBackgroundModel));
                        else
                            warning('no flag found for CPU or GPU conversion, something must have gone wrong');
                        end
                    end
                end

            elseif strcmpi(this.backgroundModelType, 'Gaussian Mixture Model')
                try
                    warndlg('this is an untested beta version')
                    tempBackgroundModel = cv.BackgroundSubtractorMOG('History',nHistory, ...
                        'NMixtures',5, 'BackgroundRatio',0.2, 'NoiseSigma',7);
                    for idx = this.frameRange(1):this.frameRange(2)
                        inputImage = this.videoObjectHandle.nextFrame(idx);
                        % learning the image
                        tempBackgroundModel.apply(inputImage, 'LearningRate',-1);
                        % assign the prelearned background
                        this.backgroundModel = tempBackgroundModel;
                        return
                    end
                catch
                    warndlg('This feature is currently not supported without mexopencv, we are working on the library free implementation, for now we will just make a median model for you')
                    this.backgroundModelType = 'Median Background Model';
                    this.calculateModelBackground;
                end
            else
                error('no model specified');
            end
            % determine normalization value
            totalFrames = numel(this.frameRange(1):this.frameRange(2));

            switch this.processingMode
                case 'GPU Speed'
                    maxNormalizationValues = zeros(numel(this.frameRange(1):this.frameRange(2)), 1, 'gpuArray');
                case 'CPU Speed'
                    maxNormalizationValues = zeros(numel(this.frameRange(1):this.frameRange(2)), 1);
                case 'CPU Accuracy'
                    maxNormalizationValues = zeros(numel(this.frameRange(1):this.frameRange(2)), 1);
            end

            for idx = this.frameRange(1):this.frameRange(2)
                inputImage = this.videoObjectHandle.nextFrame(idx);
                foregroundImage = sum( abs( inputImage-this.backgroundModel ), 3 );
                maxNormalizationValues(idx,1) = max(foregroundImage(:));
                progression = idx-this.frameRange(1)+1;
                progressionString = sprintf('%d out of %d', progression, totalFrames);
                waitbar(progression / totalFrames, progressBar, progressionString);
            end
            delete(progressBar);
            % reject the highest 90% as these might be some outliners and
            % set a normalization value.
            sortedNormalizationValues = sort(maxNormalizationValues(:));
            this.normalizationValue = sortedNormalizationValues(ceil(length(sortedNormalizationValues)*0.90));
        end
    end % methods (Access = protected)
end % end of class
