classdef VideoFilePath < matlab.mixin.Copyable
    % Very simple class that takes a filepath as input and does some small
    % checks if the file format is correct for our application (videofile)
    % and if we have read permission to access it
    
    properties
        filePath(1,:) char = '' % full path including file name and extension
    end
    
    properties (SetAccess = protected)
        name(1,:) char = ''
        extension(1,:) char = ''
        directory(1,:) char = ''
        
        isVideo(1,1) logical = false
        
        readPermission(1,1) logical = false
    end
    
    methods
        function newPath = VideoFilePath(inputPath)
            if nargin>0 && ~isempty(inputPath)
                if ischar(inputPath)
                    newPath.filePath = inputPath;
                    newPath.setPathProperties;
                else
                    warning('filePaths have to be inputted as a string!')
                end
            end
        end
    end % methods (no attributes)
    
    methods (Access = protected)
        function setPathProperties(this)
            if isempty(this.filePath)
                return
            end
            
            % can we do a faster check if it's a file or directory?
            [fileDirectory, fileName,fileExtension] = fileparts(this.filePath);
            
            if isdir(this.filePath)
                error('This is not a video file but a directory, make sure you have the full path including the extension')
            else
                % is file
                this.initialize(fileDirectory, fileName,fileExtension);
            end
        
            [status,values] = fileattrib(this.filePath);
            if ~status
                % values is the error message from file attributes
                error(values)
                return
            end
            this.readPermission = values.UserRead;
            
        end
        
        function initialize(this,fileDirectory, fileName,fileExtension)
            % no checks, internal calls only
            this.name = fileName;
            this.extension = fileExtension;
            this.directory = fileDirectory;
            
            %check extention
            isMp4 = logical(regexpi(this.extension, '.mp4'));
            isAvi = logical(regexpi(this.extension, '.avi'));
            isAsx = logical(regexpi(this.extension, '.asx'));
            isAsf = logical(regexpi(this.extension, '.asf'));
            isMPEG4 = logical(regexpi(this.extension, '.m4v'));
            isMov = logical(regexpi(this.extension, '.mov'));
            isMPEG1 = logical(regexpi(this.extension, '.mpg'));
            isWinVid = logical(regexpi(this.extension, '.wmv'));  
                
            % for some weird reason i couldnt use || here or with the initial try
            % of setting all the regexpi's in or statement so just used a
            % bunch of seperate if statements. Looks less clean but does
            % the same job
            if isMp4
                this.isVideo = true;
            elseif isAvi
                this.isVideo = true;
            elseif isAsx
                this.isVideo = true;
            elseif isAsf
                this.isVideo = true;
            elseif isMPEG4
                this.isVideo = true;
            elseif isMPEG1
                this.isVideo = true;
            elseif isWinVid
                this.isVideo = true;
            elseif isMov
                this.isVideo = true;
            else
                warning('check the extention to see if this is a supported video format');
            end
        end
    end % methods (Access = private)
end % end of class