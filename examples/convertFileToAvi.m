function convertFileToAvi
% really basic script to transform file to AVI without any error checking
[originalFileName, originalFilePath, ~] = uigetfile('*.*');
[filePath,fileName,fileExtention] = fileparts(fullfile(originalFilePath,originalFileName));
 
readerObj = VideoReader(fullfile(originalFilePath,originalFileName));
writerObj = VideoWriter(fullfile(filePath,[fileName,'.avi']),'Uncompressed AVI');
% open the file to write to 
open(writerObj);

%read and write each frame
while hasFrame(readerObj)
    frameVid = readFrame(readerObj);
    writeVideo(writerObj,frameVid);
end
close(writerObj);
end