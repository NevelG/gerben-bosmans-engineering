% You can construct a GUI by guiObject = nevelkamer.ui.MainGui
% than u load a video and construct a background model, after this u can
% make a loop

frameNumber = 800; % set a number lower than the total number of frames
counter = 1;
for idx = 1:10:FrameNumber
firstGhostingFrame = idx;
lastGhostingFrame = idx+10;
ghostedImages{counter,1} = GguiObject.backgroundModel.ghostingDetection(firstGhostingFrame, lastGhostingFrame);
counter = counter + 1;
end