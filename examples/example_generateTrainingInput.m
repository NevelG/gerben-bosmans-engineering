% after you have the data and assuming u saved the beta images in a linear cell
% array called "betaImage" and alpha in "alphaImage" you can run this
% script to generate the input for the machine learning model.
structuredElementSize = 20;
structuredElement = strel('disk', structuredElementSize);
pixelTreshold = 100;

% check for number of data points
numberOfBeta = numel(betaImages);
numberOfAlpha = numel(alphaImages);
counterBeta = 1;
figure;
h1 = imshow(betaImages{1});
title('Beta Tracks');
for idx = 1:numberOfBeta
        betaImagesNew = bwareaopen(betaImages{idx},pixelTreshold); 
        betaImagesNew = imclose(betaImagesNew, structuredElement);

     propertiesBeta = regionprops(betaImagesNew,'Perimeter','Area','MinorAxisLength','MajorAxisLength');
     if ~isempty(propertiesBeta)
         for innerIdx = 1:numel(propertiesBeta)
             if propertiesBeta(innerIdx).Area > 40
             areaBeta(counterBeta,1) = propertiesBeta(innerIdx).Area;
             perimeterBeta(counterBeta,1) = propertiesBeta(innerIdx).Perimeter;
             minorAxisBeta(counterBeta,1) = propertiesBeta(innerIdx).MinorAxisLength;
             majorAxisBeta(counterBeta,1) = propertiesBeta(innerIdx).MajorAxisLength;
          %   distanceFromCenterBeta(counterBeta,1) = norm([imageSizeBeta(2)/2, imageSizeBeta(1)/2] - propertiesBeta(innerIdx).Centroid);
             betaVector(counterBeta,1) = 0;
           %  typeRatioBeta(counterBeta,1) = (areaBeta(counterBeta,1)/perimeterBeta(counterBeta,1))*(majorAxisBeta(counterBeta,1)/minorAxisBeta(counterBeta,1));

             counterBeta = counterBeta + 1;
             end
         end
     end  
      set(h1, 'CDATA', betaImagesNew);
      drawnow
      pause(0.2);
end

counterAlpha = 1;
figure;
h2 = imshow(alphaImages{1});
title('Alpha Tracks');
for idx = 1:numel(alphaImages)
    alphaImagesNew = bwareaopen(alphaImages{idx},pixelTreshold); 
    alphaImagesNew = imclose(alphaImagesNew, structuredElement);

     propertiesAlpha = regionprops(alphaImagesNew,'Perimeter','Area','MinorAxisLength','MajorAxisLength');
     if ~isempty(propertiesAlpha)
         for innerIdx = 1:numel(propertiesAlpha)
             if propertiesAlpha(innerIdx).Area > 40
                 areaAlpha(counterAlpha,1) = propertiesAlpha(innerIdx).Area;
                 perimeterAlpha(counterAlpha,1) = propertiesAlpha(innerIdx).Perimeter;
                 minorAxisAlpha(counterAlpha,1) = propertiesAlpha(innerIdx).MinorAxisLength;
                 majorAxisAlpha(counterAlpha,1) = propertiesAlpha(innerIdx).MajorAxisLength;
              %   distanceFromCentersAlpha(counterAlpha,1) = norm([imageSizeAlpha(2)/2, imageSizeAlpha(1)/2] - propertiesAlpha(innerIdx).Centroid);
                 alphaVector(counterAlpha,1) = 1;
                % typeRatioAlpha(counterAlpha,1) = (areaAlpha(counterAlpha,1)/perimeterAlpha(counterAlpha,1))*(majorAxisAlpha(counterAlpha,1)/minorAxisAlpha(counterAlpha,1));

                 counterAlpha = counterAlpha + 1;
             end
         end
     end
      set(h2, 'CDATA', alphaImagesNew);
      drawnow
      pause(0.2);
end
% rearrange data to convert to matrix with 1 being alpha and 0 being beta
minorAxis = [minorAxisBeta;minorAxisAlpha];
majorAxis = [majorAxisBeta;majorAxisAlpha];
perimeter = [perimeterBeta;perimeterAlpha];
area = [areaBeta;areaAlpha];
%typeRatio = [typeRatioBeta; typeRatioAlpha];

% do it with a matrix for reduced computation time
trainingMatrix = [minorAxis, majorAxis, perimeter, area, [betaVector; alphaVector]];